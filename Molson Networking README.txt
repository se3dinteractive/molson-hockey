Molson Networking README

This project runs on a basic peer-to-peer system using .NET networking components to do
host discovery before connecting and communicating using Unity's built-in networking.

To start a game, users can choose to either Host a new game or Join an existing game.
The relevant scripts can be found in Assets/_Molson/Scripts/Networking/. HostBroadcast 
takes care of broadcasting host data, and ListenForHosts takes care of displaying that
data to clients, and allowing them to choose and connect to a host.

Once the game is started, the networking is mostly done by the NetworkView component.
This must be attached to any object that will be synched across the network. The object
in question should then be spawned using Network.Instantiate, which will spawn the 
object for all connected players, and associate those objects with each other. Depending
on how complicated the game is, you can choose to have Unity automatically take care of
network sync (by setting the Observed field in the NetworkView script to, say, a Transform),
however to utilize prediction and smoothing techniques, you'll need to implement a custom 
solution.

Basic game setup can be seen in Assets/_Molson/Scripts/Controllers/GameController.cs, and 
sample custom networking views can be seen in PlayerController.cs and PuckController.cs in 
the same folder.

Generally, you'll want to use ReliableDeltaCompressed for your NetworkView's State 
Synchronization field (this means the object will always report only the values that have
changed since the last sent frame). You can adjust the network send rate in Edit>Project 
Settings>Network.