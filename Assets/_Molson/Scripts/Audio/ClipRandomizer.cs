﻿using UnityEngine;
using System.Collections;

public class ClipRandomizer : MonoBehaviour {

	public AudioClip[] clips;
	public bool loop;

	AudioSource source;
	bool isPlaying;

	// Use this for initialization
	void Start() {
		source = GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void Update() {
		if (isPlaying && loop && !source.isPlaying) {
			PlaySound();
		}
	}

	public void PlaySound() {
		int index = Random.Range(0, clips.Length);
		source.clip = clips[index];
		source.Play();
	}

	public void StartPlaying() {
		isPlaying = true;
	}

	public void StopPlaying() {
		isPlaying = false;
	}

	public void SetShouldLoop(bool _shouldLoop) {
		loop = _shouldLoop;
	}
}
