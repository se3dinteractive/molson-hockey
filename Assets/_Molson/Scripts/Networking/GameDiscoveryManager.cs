﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class GameDiscoveryManager : MonoBehaviour {

	private struct ServerInfo {
		public string signature;
		public GameObject displayObject;
	};

	public int discoveryPort = 19386;

	public float timeBetweenBroadcasts = 5.0f;
	public string hostGameName = "New Game";
	public UserManager userMgr;

	public GameObject gameInfoDisplayPrefab;
	public Transform gameInfoDisplayParent;


	private UdpClient udpClient;
	
	private IPEndPoint discoveryAddress;
	private List<string> serverInfoToDisplay;
	private List<ServerInfo> activeServerSignatures;
	private UIStateController stateMgr;

	private bool isHost = false;

	void Start() {
		stateMgr = FindObjectOfType<UIStateController>();
	}

	void Update() {
		// Instantiate can only be called from the main thread,
		// so cache received strings and display them from here
		if (!isHost && serverInfoToDisplay != null && serverInfoToDisplay.Count > 0) {
			for (int i = 0; i < serverInfoToDisplay.Count; i++) {
				DisplayServerString(serverInfoToDisplay[i]);
			}
			serverInfoToDisplay.Clear();
		}
	}

	void OnDestroy() {
		CleanUp();
		if (isHost) {
			Network.Disconnect();
		}
	}

	/******************
	 * 
	 * CLIENT CODE
	 * 
	 ******************/

	public void StartListeningForHosts() {
		CleanUp();
		isHost = false;
		activeServerSignatures = new List<ServerInfo>();
		serverInfoToDisplay = new List<string>();
		try {
			discoveryAddress = new IPEndPoint(IPAddress.Any, discoveryPort);

			udpClient = new UdpClient(discoveryPort);
			udpClient.BeginReceive(new AsyncCallback(ReceiveData), null);
		} catch (SocketException _ex) {
			Debug.LogError(_ex.Message);
		}
	}

	private void ReceiveData(IAsyncResult _result) {
		byte[] receivedData;
		if (udpClient != null) {
			receivedData = udpClient.EndReceive(_result, ref discoveryAddress);
		} else {
			return;
		}

		udpClient.BeginReceive(new AsyncCallback(ReceiveData), null);

		ProcessServerString(Encoding.ASCII.GetString(receivedData));
	}

	private void ProcessServerString(string _serverString) {
		bool alreadyReceived = false;
		for (int i = 0; i < activeServerSignatures.Count; i++) {
			if (activeServerSignatures[i].signature == _serverString) {
				alreadyReceived = true;
				break;
			}
		}

		if (!alreadyReceived) {
			serverInfoToDisplay.Add(_serverString);
			//DisplayServerString(_serverString);
		}
	}

	private void DisplayServerString(string _serverString) {
		int ipSeparator = _serverString.IndexOf('*', 0);
		int usernameSeparator = _serverString.IndexOf('*', ipSeparator + 1);

		string ip = _serverString.Substring(0, ipSeparator - 1);
		string username = _serverString.Substring(ipSeparator + 1, usernameSeparator - ipSeparator - 1);
		string gameName = _serverString.Substring(usernameSeparator + 1, _serverString.Length - usernameSeparator - 1);

		ServerInfo newGameInfo;
		newGameInfo.signature = _serverString;
		newGameInfo.displayObject = (GameObject)GameObject.Instantiate(gameInfoDisplayPrefab);
		GameInfoDisplay infoDisplay = newGameInfo.displayObject.GetComponent<GameInfoDisplay>();

		infoDisplay.SetupDisplay(	gameInfoDisplayParent, 
									new Vector2(10, -50 - (50 * activeServerSignatures.Count)),
									() => ConnectToServer(ip),
									username,
									gameName);

		activeServerSignatures.Add(newGameInfo);
	}

	private void ConnectToServer(string _ip) {
		Network.Connect(_ip, discoveryPort - 2);
		stateMgr.ChangeStateTo("GameLobbyScreen");
	}

	/******************
	 * 
	 * HOST CODE
	 * 
	 ******************/

	public void StartBroadcasting() {
		CleanUp();
		isHost = true;
		try {
			Network.InitializeServer(1, discoveryPort - 2, false);

			discoveryAddress = new IPEndPoint(IPAddress.Broadcast, discoveryPort);

			udpClient = new UdpClient(discoveryPort - 1, AddressFamily.InterNetwork);
			udpClient.Connect(discoveryAddress);

			InvokeRepeating("BroadcastData", 0, timeBetweenBroadcasts);
		} catch (SocketException _ex) {
			Debug.LogError(_ex.Message);
		}
	}

	private void BroadcastData() {
		string message = Network.player.ipAddress + " * " + userMgr.username + " * " + hostGameName;
		udpClient.Send(Encoding.ASCII.GetBytes(message), message.Length);
	}

	public void CleanUp() {
		if (udpClient != null) {
			udpClient.Close();
		}

		if (isHost) {
			CancelInvoke("BroadcastData");
		} else {
			if (activeServerSignatures != null) {
				for (int i = 0; i < activeServerSignatures.Count; i++) {
					UnityEngine.Object.Destroy(activeServerSignatures[i].displayObject);
				}
				activeServerSignatures.Clear();
				activeServerSignatures = null;
			}

			if (serverInfoToDisplay != null) {
				serverInfoToDisplay.Clear();
				serverInfoToDisplay = null;
			}
		}
	}
}
