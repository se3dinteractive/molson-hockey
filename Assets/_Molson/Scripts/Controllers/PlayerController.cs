﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public ClipRandomizer skateSound;
	public AudioSource shotSound;

	private AIController aiController;
	private NetworkView myNetworkView;
	private PlayerInputController playerInputController;
	private PlayerModel playerModel;
	private PlayerView playerView;
	private Rigidbody myRigidbody;

	private GameTimer gameTimer;
	private PuckController puckController;
	private RinkModel rinkModel;
	private TeamController teamController;
	private ShooterCameraController shootCamController;

	private Vector3 targetForward;
	private float currentSpeed;
	private Vector3 curAcceleration;
	private Vector3 curVelocity;

	private float elapsedSpeedDecayTime;
	private float startDecaySpeed;

	private bool isHitting;
	private float elapsedHitTime;
	private GameObject hitTarget;

	private bool isWindingUp;
	private float elapsedWindUpTime;
	private bool shouldShoot;
	private bool shouldPass;

	private bool isPuckCooldownActive;
	private float elapsedPuckCooldownTime;

	private bool isInitialized;

	private bool isPlayerControlled;

	private bool isGoalie;
	private GoalieController goalieController;

	private Vector3 targetShotPos;

	/*********************
	 * Monobehaviour Hooks
	 *********************/

	void Start() {
		Initialize();
	}
	
	void Update() {
		if (!playerModel.isBeingHit && !gameTimer.IsGamePaused()) {
			if (playerModel.activeInputMagnitude <= 0) {
				elapsedSpeedDecayTime += Time.deltaTime;
			}

			if (isWindingUp) {
				if (!HasPuck()) {
					CancelShot();
				} else {
					elapsedWindUpTime += Time.deltaTime;
					float lerp = Mathf.Min(elapsedWindUpTime / playerModel.maxShootWindUpTime, 1.0f);
					playerView.UpdatePowerBar(lerp);
				}
			}

			if (isHitting) {
				targetForward = transform.forward;
				elapsedHitTime += Time.deltaTime;
				if (elapsedHitTime >= playerModel.hitDuration) {
					isHitting = false;
					if (hitTarget != null) {
						hitTarget.SendMessage("OnHitEnd", SendMessageOptions.DontRequireReceiver);
						hitTarget = null;
					}
				}
			}

			if (isPuckCooldownActive) {
				elapsedPuckCooldownTime += Time.deltaTime;
				if (elapsedPuckCooldownTime >= playerModel.puckCooldownTime) {
					isPuckCooldownActive = false;
				}
			}

			if (shouldShoot || shouldPass) {
				ReleasePuck();
			}

			if (isPlayerControlled) {
				playerModel.activeInputMagnitude = playerInputController.GetMovementMagnitude();
				targetForward = playerInputController.GetDirection();
			}
		}
	}

	void FixedUpdate() {
		if (isPlayerControlled && !playerModel.isBeingHit && !gameTimer.IsGamePaused()) {
			if (isGoalie && !HasPuck()) {
				StepGoalieMovement();
			} else {
				StepMovement();
			}
		}
	}

	void OnTriggerEnter(Collider _collider) {
		if (!isPuckCooldownActive && !HasPuck() && _collider.gameObject.layer == Config.PUCK_LAYER) {
			if (Config.isOnline && (Network.isServer && playerModel.teamLayer == Config.HOME_TEAM_LAYER || Network.isClient && playerModel.teamLayer == Config.AWAY_TEAM_LAYER) || !Config.isOnline) {
				puckController = _collider.gameObject.GetComponent<PuckController>();
				if (Config.isOnline) {
					myNetworkView.RPC("ClaimPuck", RPCMode.All);
					teamController.SetActivePlayer(playerModel.TeamID);
				} else {
					ClaimPuck();
					if (playerModel.teamLayer == Config.HOME_TEAM_LAYER) {
						teamController.SetActivePlayer(playerModel.TeamID);
					}
				}
			}
		}
	}

	void OnCollisionEnter(Collision _collision) {
		if (isHitting && hitTarget == null && _collision.collider.transform.parent) {
			
			hitTarget = _collision.collider.transform.parent.gameObject;
			PlayerModel otherPlayerModel = hitTarget.GetComponent<PlayerModel>();
			if (otherPlayerModel != null && otherPlayerModel.teamLayer != playerModel.teamLayer) {
				hitTarget.SendMessage("OnReceiveHit");
			} else {
				hitTarget = null;
			}
		}
	}

	/*******************
	 * PUBLIC HOOKS
	 *******************/

	public void StartShootWindUp() {
		if (HasPuck() && !isWindingUp) {
			targetShotPos = rinkModel.GetAttackingNetPosition(playerModel.teamLayer);
			isWindingUp = true;
			elapsedWindUpTime = 0;
			playerView.EnablePowerBar(PlayerView.ShotType.SHOT);
			if (playerModel.useAimShot) {
				Time.timeScale = playerModel.aimingTimeScale;
				shootCamController.SetUpCamera(playerModel);
			}

			if (isPlayerControlled) {
				SeedAnalytics.GetInstance().StartTimer("shot_wind_up");
			}
		}
	}

	public void ReleaseShot() {
		if (HasPuck()) {
			shouldShoot = true;
			isWindingUp = false;
			playerView.DisablePowerBar();
			if (playerModel.useAimShot) {
				Time.timeScale = 1.0f;
				shootCamController.DisableCamera();
			}

			if (isPlayerControlled) {
				SeedAnalytics.GetInstance().StopTimer("shot_wind_up");
			}
		}
	}

	public void CancelShot() {
		isWindingUp = false;
		playerView.DisablePowerBar();
		if (playerModel.useAimShot) {
			shootCamController.DisableCamera();
		}

		if (isPlayerControlled) {
			SeedAnalytics.GetInstance().StopTimer("shot_wind_up");
		}
	}

	public void ReleasePass() {
		if (HasPuck()) {
			shouldPass = true;
			if (playerModel.useAimShot) {
				shootCamController.DisableCamera();
			}
		}
	}

	public void OnReceiveHit() {
		isWindingUp = false;
		isHitting = false;
		myRigidbody.velocity = Vector3.zero;
		ReleasePuck();

		if (isPlayerControlled) {
			SeedAnalytics.GetInstance().ReportEvent("got_hit");
		}
	}

	public void StartHit() {
		if (!isHitting && !isGoalie) {
			if (Config.isOnline) {
				myNetworkView.RPC("ProcessHit", RPCMode.All);
			} else {
				ProcessHit();
			}

			if (isPlayerControlled) {
				SeedAnalytics.GetInstance().ReportEvent("started_hit");
			}
		} else if (isGoalie) {
			if (goalieController == null) {
				goalieController = GetComponent<GoalieController>();
			}
			goalieController.TogglePosition();
		}
	}

	public void PrepareForFaceoff(Transform _target) {
		if (myRigidbody == null) {
			myRigidbody = GetComponent<Rigidbody>();
		}
		myRigidbody.velocity = Vector3.zero;
		myRigidbody.isKinematic = true;
		currentSpeed = 0;

		transform.position = _target.position;
		transform.forward = _target.forward;
		myRigidbody.isKinematic = false;

		if (enabled) {
			playerView.DisablePowerBar();
			playerView.DisablePlayerHighlight();
			playerView.ShowNoPuckControls();
		}
	}

	public void ForceTrigger(Collider _collider) {
		OnTriggerEnter(_collider);
	}

	public void SetIsPlayerControlled(bool _isPlayerControlled) {
		isPlayerControlled = _isPlayerControlled;
		aiController.enabled = !isPlayerControlled;
	}

	public void SetIsGoalie(bool _isGoalie) {
		isGoalie = _isGoalie;
		if (isGoalie && goalieController == null) {
			goalieController = GetComponent<GoalieController>();
		}
	}

	public bool HasPuck() {
		return playerModel.puckParent.childCount > 0;
	}

	public void SetTargetShotPos(Vector3 newTarget) {
		targetShotPos = newTarget;
	}

	/***************************************
	 * PRIVATE NETWORK AND LOGIC PROCESSORS
	 * *************************************/

	[RPC]
	private void ClaimPuck() {
		if (puckController == null) {
			puckController = FindObjectOfType<PuckController>();
		}
		puckController.Claim(playerModel.puckParent);
	}

	[RPC]
	private void ShootPuck(Vector3 _force) {
		puckController.Shoot(_force, playerModel.puckParent);
	}

	[RPC]
	private void ProcessHit() {
		if (!isHitting) {
			isHitting = true;
			myRigidbody.mass = 100.0f;
			elapsedHitTime = 0.0f;
		}
	}

	private void ReleasePuck() {
		if (shotSound) shotSound.Play();
		Vector3 force = Vector3.zero;

		if (shouldPass) {
			teamController.ResetAllHighlights();
			PlayerModel model = teamController.GetClosestPlayerModel(gameObject, true);
			if (model != null) {
				if (isPlayerControlled) {
					SeedAnalytics.GetInstance().ReportEvent("pass");
				}
				force = model.puckParent.position - playerModel.puckParent.position;
				force.y = 0;
				force.Normalize();
				force = force * playerModel.passSpeed;
			} else {
				shouldShoot = true;
			}
			
		}

		if (shouldShoot) {
			if (isPlayerControlled) {
				SeedAnalytics.GetInstance().ReportEvent("shot");
			}
			float lerp = Mathf.Min(elapsedWindUpTime / playerModel.maxShootWindUpTime, 1.0f);
			force = Vector3.Normalize(targetShotPos - playerModel.puckParent.position) * (playerModel.minShootSpeed + (playerModel.maxShootSpeed - playerModel.minShootSpeed) * lerp);
			/*float lerp = Mathf.Min(elapsedWindUpTime / playerModel.maxShootWindUpTime, 1.0f);
			force = Vector3.Normalize(rinkModel.GetAttackingNetPosition(playerModel.teamLayer) - playerModel.puckParent.position);
			force = Vector3.Normalize(Quaternion.Euler(playerModel.shotVerticalAngle * lerp, 0, 0) * force) * (playerModel.minShootSpeed + (playerModel.maxShootSpeed - playerModel.minShootSpeed) * lerp);*/
		}

		shouldShoot = shouldPass = false;
		isPuckCooldownActive = true;
		elapsedPuckCooldownTime = 0.0f;

		if (Config.isOnline) {
			myNetworkView.RPC("ShootPuck", RPCMode.All, force);
		} else {
			ShootPuck(force);
		}
	}

	private void StepMovement() {
		if (transform.forward != targetForward) {
			transform.forward = Vector3.MoveTowards(transform.forward, targetForward, playerModel.rotationCorrectionSpeed * Time.deltaTime);
		}

		if (isHitting) {
			currentSpeed = playerModel.hitSpeed;
			if (skateSound) skateSound.StartPlaying();
		} else if (playerModel.activeInputMagnitude > 0) {
			curAcceleration = targetForward * playerModel.acceleration * playerModel.activeInputMagnitude;
			curVelocity = curAcceleration * Time.deltaTime;
			currentSpeed += curVelocity.magnitude;
			if (currentSpeed > playerModel.movementSpeed) {
				currentSpeed = playerModel.movementSpeed;
			}
			startDecaySpeed = currentSpeed;
			elapsedSpeedDecayTime = 0.0f;
			if (skateSound) skateSound.StartPlaying();
		} else {
			if (skateSound) skateSound.StopPlaying();
			if (elapsedSpeedDecayTime > playerModel.movementSpeedDecayDuration) elapsedSpeedDecayTime = playerModel.movementSpeedDecayDuration;
			currentSpeed = startDecaySpeed - (elapsedSpeedDecayTime / playerModel.movementSpeedDecayDuration) * startDecaySpeed;
		}

		myRigidbody.velocity = transform.forward * currentSpeed;
	}

	private void StepGoalieMovement() {
		Vector3 puckDirection = Vector3.Normalize(puckController.transform.position - transform.position);
		puckDirection.y = 0;
		puckDirection.Normalize();
		if (transform.forward != puckDirection) {
			transform.forward = Vector3.MoveTowards(transform.forward, puckDirection, playerModel.rotationCorrectionSpeed * Time.deltaTime);
		}

		if (playerModel.activeInputMagnitude > 0) {
			currentSpeed = playerModel.movementSpeed * playerModel.activeInputMagnitude;
			startDecaySpeed = currentSpeed;
			elapsedSpeedDecayTime = 0.0f;
		} else {
			if (elapsedSpeedDecayTime > playerModel.movementSpeedDecayDuration) elapsedSpeedDecayTime = playerModel.movementSpeedDecayDuration;
			currentSpeed = startDecaySpeed - (elapsedSpeedDecayTime / playerModel.movementSpeedDecayDuration) * startDecaySpeed;
		}

		myRigidbody.velocity = targetForward * currentSpeed * playerModel.curSpeedMod;
		transform.forward = goalieController.ConstrainForward();
	}

	public void Initialize() {
		if (!isInitialized) {
			if (transform.parent == null) {
				string targetParent = (Config.isOnline && !Network.isServer) ? "Home Team" : "Away Team";
				transform.parent = GameObject.Find(targetParent).transform;
			}

			aiController = GetComponent<AIController>();
			myNetworkView = GetComponent<NetworkView>();
			playerInputController = GetComponent<PlayerInputController>();
			playerModel = GetComponent<PlayerModel>();
			playerView = GetComponent<PlayerView>();
			myRigidbody = GetComponent<Rigidbody>();

			gameTimer = FindObjectOfType<GameTimer>();
			puckController = FindObjectOfType<PuckController>();
			rinkModel = FindObjectOfType<RinkModel>();
			teamController = transform.parent.gameObject.GetComponent<TeamController>();
			shootCamController = FindObjectOfType<ShooterCameraController>();

			playerView.DisablePowerBar();
			targetForward = transform.forward;

			elapsedSpeedDecayTime = playerModel.movementSpeedDecayDuration;

			isInitialized = true;
		}
	}
}
