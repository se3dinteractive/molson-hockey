﻿using UnityEngine;
using System.Collections;

public class PlayerInputController : MonoBehaviour {

	public JoystickController joystick;

	private float curInputMagnitude;
	private Vector3 curDirection;
	private PlayerController playerController;
	private TeamController teamController;

	// Use this for initialization
	void Start() {
		playerController = GetComponent<PlayerController>();
		teamController = transform.parent.gameObject.GetComponent<TeamController>();
	}

	// Update is called once per frame
	void Update() {
		if (joystick != null) {
			Vector2 joystickDir = joystick.GetDirection();
			curDirection = new Vector3(joystickDir.x, 0, joystickDir.y);
			curInputMagnitude = joystick.GetSpeedMagnitude();
			if (curInputMagnitude > 1) curInputMagnitude = 1;
		}

		if (teamController.gameObject.layer == Config.HOME_TEAM_LAYER) {
			if (Input.GetKeyDown("a")) {
				if (playerController.HasPuck()) {
					playerController.ReleasePass();
				} else {
					teamController.SwitchToClosestPlayer();
				}
			}
			if (Input.GetKeyDown("d")) {
				if (playerController.HasPuck()) {
					playerController.StartShootWindUp();
				} else {
					playerController.StartHit();
				}
			} else if (Input.GetKeyUp("d")) {
				if (playerController.HasPuck()) {
					playerController.ReleaseShot();
				}
			}
		}
	}

	public float GetMovementMagnitude() {
		return curInputMagnitude;
	}

	public Vector3 GetDirection() {
		return curDirection;
	}
}
