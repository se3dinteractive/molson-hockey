﻿using UnityEngine;
using System.Collections;

public class ShooterCameraController : MonoBehaviour {

	public GameObject shooterCam;
	public LineRenderer aimingReticule;

	private RinkModel rinkModel;
	private Vector3 netPos;
	private PlayerModel activePlayer;
	private PlayerController playerController;
	private Camera mainCamera;
	private Vector3 targetPos;
	private int activeTouch = -1;

	// Use this for initialization
	void Start() {
		rinkModel = FindObjectOfType<RinkModel>();
		mainCamera = Camera.main;
	}

	// Update is called once per frame
	void Update() {
		if (shooterCam.activeSelf) {
			if (Input.GetMouseButton(0) || Input.touchCount > 0) {
				if (Input.touchCount > 0 && activeTouch == -1) {
					for (int i = 0; i < Input.touchCount; i++) {
						if (Input.GetTouch(i).phase == TouchPhase.Began) {
							activeTouch = i;
						}
					}
				}
				Vector2 point = (activeTouch == -1) ? (Vector2)Input.mousePosition : Input.GetTouch(activeTouch).position;
				RaycastHit hit;
				int layer = ~(1 << Config.HOME_TEAM_ZONE_LAYER | 1 << Config.AWAY_TEAM_ZONE_LAYER);
				if (Physics.Raycast(shooterCam.GetComponent<Camera>().ScreenPointToRay(point), out hit, 500.0f, layer)) {
					targetPos = hit.point;
					playerController.SetTargetShotPos(targetPos);
				}
			}

			shooterCam.transform.LookAt(netPos);
			PlaceCrosshair(Vector3.Normalize(targetPos - activePlayer.puckParent.position));

			if (activeTouch > 0 && Input.GetTouch(activeTouch).phase == TouchPhase.Ended) {
				activeTouch = -1;
			}
			//Debug.DrawLine(activePlayer.puckParent.position, Vector3.Normalize(netPos - activePlayer.puckParent.position) * 10.0f, Color.green);
		}
	}

	public void SetUpCamera(PlayerModel target) {
		activePlayer = target;
		playerController = activePlayer.gameObject.GetComponent<PlayerController>();

		mainCamera.enabled = false;

		shooterCam.transform.parent = activePlayer.shooterCamTarget;
		shooterCam.transform.localPosition = Vector3.zero;

		netPos = rinkModel.GetAttackingNetPosition(activePlayer.teamLayer);
		targetPos = netPos;
		shooterCam.transform.LookAt(netPos);
		shooterCam.SetActive(true);

		PlaceCrosshair(shooterCam.transform.forward);
		activeTouch = -1;
	}

	public void DisableCamera() {
		shooterCam.SetActive(false);
		shooterCam.transform.parent = transform;
		aimingReticule.gameObject.SetActive(false);
		mainCamera.enabled = true;
	}

	void PlaceCrosshair(Vector3 direction) {
		float distance = (netPos - activePlayer.puckParent.position).magnitude;
		RaycastHit hit;
		int layer = ~(1 << Config.HOME_TEAM_ZONE_LAYER | 1 << Config.AWAY_TEAM_ZONE_LAYER);
		if (Physics.Raycast(activePlayer.puckParent.position, direction, out hit, 500.0f, layer)) {
			aimingReticule.transform.position = hit.point;
			aimingReticule.SetPosition(0, activePlayer.puckParent.position);
			aimingReticule.SetPosition(1, aimingReticule.transform.position);
		}

		aimingReticule.transform.LookAt(shooterCam.transform);

		aimingReticule.gameObject.SetActive(true);
	}
}
