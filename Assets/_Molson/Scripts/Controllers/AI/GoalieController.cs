﻿using UnityEngine;
using System.Collections;

public class GoalieController : MonoBehaviour, IPuckEventListenerInterface {

	public float movementRadius = 2.0f;
	public float reflexDelayTime = 0.4f;

	public float maxHoldTime = 2.0f;

	public float downSpeedMod = 0.2f;

	public float reactionTime = 0.1f;

	public float savePercentage = 0.98f;
	public float upSavePercentage = 0.86f;

	public float minGoDownDistance = 0.0f;
	public float maxGoDownDistance = 8.0f;

	AIController aiController;
	PlayerModel playerModel;
	PlayerController playerController;
	RulesEnforcer rulesEnforcer;
	RinkModel rinkModel;
	GameTimer gameTimer;

	[HideInInspector]
	public Vector3 startPos;

	bool isTracking = false;
	bool isInPosition = false;

	bool wasShot = false;
	float elapsedTimeSinceShot;

	bool hasPuck = false;
	float elapsedHoldTime = 0.0f;

	Collider[] colliders;
	bool collidersEnabled = true;

	void Start() {
		FindObjectOfType<PuckEventManager>().AddListener(this);

		colliders = GetComponentsInChildren<Collider>();

		aiController = GetComponent<AIController>();
		playerModel = GetComponent<PlayerModel>();
		playerController = GetComponent<PlayerController>();
		rulesEnforcer = FindObjectOfType<RulesEnforcer>();
		gameTimer = FindObjectOfType<GameTimer>();
		rinkModel = FindObjectOfType<RinkModel>();

		startPos = transform.position;
	}

	void Update() {

		if (!hasPuck && playerModel.puckParent.childCount > 0) {
			hasPuck = true;
			elapsedHoldTime = 0.0f;
			GetUp();
		} else if (hasPuck && playerModel.puckParent.childCount == 0) {
			hasPuck = false;
		}

		if (!gameTimer.IsGamePaused() && hasPuck) {
			elapsedHoldTime += Time.deltaTime;

			if (elapsedHoldTime >= maxHoldTime) {
				//rulesEnforcer.OnGoalieHold();
				//hasPuck = false;
			} else if (aiController.enabled) {
				bool passPuck = Random.Range(0.0f, 1.0f) >= (1.0f - elapsedHoldTime / maxHoldTime);
				if (passPuck) {
					playerController.ReleasePass();
					hasPuck = false;
				}
			}
		} else if (!gameTimer.IsGamePaused() && !hasPuck) {
			TrackPuck();

			float puckDistance = (aiController.puckController.transform.position - transform.position).magnitude;
			if (puckDistance <= maxGoDownDistance) {
				float chance = Mathf.Max(maxGoDownDistance - puckDistance, 0.1f);
				if (Random.Range(0.0f, 1.0f) <= chance) {
					GoDown();
				}
			} else {
				GetUp();
			}

			if (wasShot) {
				elapsedTimeSinceShot += Time.deltaTime;
				if (elapsedTimeSinceShot >= reactionTime) {
					GoDown();
					if (!collidersEnabled) CalcSave();
					wasShot = false;
				}
			}
		}
	}

	void LateUpdate() {
		transform.position = ConstrainToCrease(transform.position);
		transform.forward = ConstrainForward();
	}

	void OnGetPuck() {

	}

	void OnLosePuck() {

	}

	public void TogglePosition() {
		if (isInPosition) {
			GetUp();
		} else {
			GoDown();
		}
	}

	void GoDown() {
		playerModel.curSpeedMod = downSpeedMod;
		isInPosition = true;
	}

	void GetUp() {
		playerModel.curSpeedMod = 1.0f;
		isInPosition = false;
	}

	void TrackPuck() {
		if (!isTracking) {
			isTracking = true;
			aiController.ClearAllActions();

			StartTrackingAction();
		}
	}

	void ClearController() {
		aiController.ClearAllActions();
		isTracking = false;
	}

	void ResetPosition() {
		isTracking = false;
		aiController.ClearAllActions();

		aiController.QueueAction(new MoveToPoint(startPos, 0.5f));
	}

	public void OnZoneEnter(PuckEventManager.IceZone _zone) {
		
	}

	public void OnZoneExit(PuckEventManager.IceZone _zone) {

	}

	public void OnClaimed(int _teamLayer) {
		GetUp();
	}

	private void DisableColliders() {
		collidersEnabled = false;
		for (int i = 0; i < colliders.Length; i++) {
			colliders[i].enabled = false;
		}
	}

	private void EnableColliders() {
		collidersEnabled = true;
		for (int i = 0; i < colliders.Length; i++) {
			colliders[i].enabled = true;
		}
	}

	private void CalcSave() {
		float c = Random.Range(0.0f, 1.0f);
		if (c > ((isInPosition) ? savePercentage : upSavePercentage)) {
			DisableColliders();
		}
	}

	public void OnShot(int _teamLayer) {
		if (_teamLayer != playerModel.teamLayer) {
			wasShot = true;
			elapsedTimeSinceShot = 0;
			CalcSave();
		}
	}

	public void OnReset() {
		ResetPosition();
		EnableColliders();
	}

	public Vector3 ConstrainToCrease(Vector3 _pos) {
		Vector3 finalPos = _pos;

		if ((_pos - startPos).magnitude >= movementRadius) {
			finalPos = startPos + Vector3.Normalize(_pos - startPos) * movementRadius;
		}

		return finalPos;
	}

	public Vector3 ConstrainForward() {
		Vector3 finalForward = transform.forward;

		Vector3 trueForward = rinkModel.GetAttackingNetPosition(playerModel.teamLayer) - transform.position;
		float forwardDot = Vector3.Dot(finalForward, trueForward);
		if (forwardDot < 0) {
			Vector3 right = Vector3.Cross(trueForward, transform.up);
			float rightDot = Vector3.Dot(finalForward, right);
			if (rightDot >= 0) {
				finalForward = right;
			} else {
				finalForward = -right;
			}
		}

		finalForward.y = 0;

		return finalForward;
	}

	public void StartTrackingAction(bool _onlyLook = false) {
		aiController.QueueAction(new TrackPuckAction(aiController.puckController.transform, movementRadius, startPos, _onlyLook));
	}

}
