﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIController : MonoBehaviour {

	public PuckController puckController;

	Queue<AIAction> actions;

	AIAction currentAction;
	PlayerModel playerModel;
	Rigidbody myRigidbody;

	GameTimer gameTimer;

	private bool isLocked;

	// Use this for initialization
	void Start() {
		actions = new Queue<AIAction>();
		currentAction = null;

		playerModel = GetComponent<PlayerModel>();
		myRigidbody = GetComponent<Rigidbody>();
		gameTimer = FindObjectOfType<GameTimer>();
		puckController = FindObjectOfType<PuckController>();
	}

	void Update() {
		if (!playerModel.isBeingHit && !gameTimer.IsGamePaused()) {
			if (currentAction != null) {
				currentAction.Update(playerModel, myRigidbody);
				if (currentAction != null && currentAction.IsFinished()) {
					currentAction.OnFinish(playerModel, myRigidbody);
					currentAction = null;
				}
			}

			if (currentAction == null && actions.Count > 0) {
				currentAction = actions.Dequeue();
			}
		}
	}

	public void QueueAction(AIAction _newAction) {
		if (!isLocked && actions != null) {
			actions.Enqueue(_newAction);
		}
	}

	public void ClearAllActions() {
		if (!isLocked && actions != null) {
			actions.Clear();
			currentAction = null;
			myRigidbody.velocity = Vector3.zero;
		}
	}

	public void SetIsLocked(bool _isLocked) {
		isLocked = _isLocked;
	}
}
