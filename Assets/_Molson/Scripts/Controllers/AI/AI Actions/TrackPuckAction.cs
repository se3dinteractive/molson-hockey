﻿using UnityEngine;
using System.Collections;

public class TrackPuckAction : AIAction {

	float radius;
	Transform target;
	Vector3 startPos;

	Vector3[] lastPuckPos;
	float elapsedTime;

	private bool onlyLook;

	public TrackPuckAction(Transform _target, float _radius, Vector3 _startPos, bool _onlyLook = false) {
		target = _target;
		radius = _radius;
		startPos = _startPos;

		lastPuckPos = new Vector3[4];
		for (int i = 0; i < lastPuckPos.Length; i++) {
			lastPuckPos[i] = target.position;
		}

		onlyLook = _onlyLook;
	}

	public override void Update(PlayerModel _model, Rigidbody _rigidbody) {
		Vector3 puckPos = lastPuckPos[0];
		for (int i = 0; i < lastPuckPos.Length - 1; i++) {
			lastPuckPos[i] = lastPuckPos[i + 1];
		}
		lastPuckPos[lastPuckPos.Length - 1] = target.position;

		puckPos.y = _model.transform.position.y;

		puckPos = puckPos - _model.transform.position;
		float distance = Mathf.Min(puckPos.magnitude, radius);

		Vector3 targetForward = Vector3.Normalize(puckPos);
		targetForward.y = 0;
		Vector3 targetPos = startPos + targetForward * distance;

		if (!onlyLook) {
			_model.transform.position = Vector3.MoveTowards(_model.transform.position, targetPos, _model.movementSpeed * Time.deltaTime * _model.curSpeedMod);
			_model.activeInputMagnitude = Mathf.Min((_model.transform.position - targetPos).magnitude, 1.0f);
		}
		_model.transform.forward = Vector3.MoveTowards(_model.transform.forward, targetForward, _model.rotationCorrectionSpeed * Time.deltaTime);
	}
}
