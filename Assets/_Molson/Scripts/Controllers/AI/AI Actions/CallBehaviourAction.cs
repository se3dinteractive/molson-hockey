﻿using UnityEngine;
using System.Collections;

public class CallBehaviourAction : AIAction {

	public delegate void BehaviourDelegate();

	BehaviourDelegate behaviour;

	public CallBehaviourAction(BehaviourDelegate _delegate) {
		behaviour = _delegate;
	}

	public override void Update(PlayerModel _model, Rigidbody _rigidbody) {
		if (!isFinished) {
			behaviour();
			isFinished = true;
		}
	}

}
