﻿using UnityEngine;
using System.Collections;

public class FollowObject : AIAction {

	Transform target;

	public FollowObject(Transform _target) {
		target = _target;
	}

	public override void Update(PlayerModel _model, Rigidbody _rigidbody) {
		Vector3 targetDirection = target.position - _model.transform.position;
		targetDirection.y = 0;

		if (targetDirection.magnitude <= 0.1f) {
			isFinished = true;
		}

		_model.activeInputMagnitude = 1.0f;
		_model.transform.forward = Vector3.MoveTowards(_model.transform.forward, targetDirection, _model.rotationCorrectionSpeed * Time.deltaTime);
		_rigidbody.velocity = _model.transform.forward * _model.movementSpeed;
		
	}
}
