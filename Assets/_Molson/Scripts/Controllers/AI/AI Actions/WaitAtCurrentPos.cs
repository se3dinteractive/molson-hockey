﻿using UnityEngine;
using System.Collections;

public class WaitAtCurrentPos : AIAction {

	float duration;

	float elapsedTime;

	public WaitAtCurrentPos(float _duration) {
		duration = _duration;
	}

	public override void Update(PlayerModel _model, Rigidbody _rigidbody) {
		elapsedTime += Time.deltaTime;
		if (elapsedTime >= duration) {
			isFinished = true;
		}
		_model.activeInputMagnitude = 0.0f;
	}

}
