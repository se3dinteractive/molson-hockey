﻿using UnityEngine;
using System.Collections;

public abstract class AIAction {

	protected bool isFinished;

	public abstract void Update(PlayerModel _model, Rigidbody _rigidbody);

	public virtual bool IsFinished() {
		return isFinished;
	}

	public virtual void OnFinish(PlayerModel _model, Rigidbody _rigidbody) {
		_rigidbody.velocity = Vector3.zero;
		_model.activeInputMagnitude = 0;
	}

}
