﻿using UnityEngine;
using System.Collections;

public class MoveToPoint : AIAction {

	public Vector3 target;
	public float minAcceptableDistance = 1.0f;

	public MoveToPoint(Vector3 _target, float _minAcceptableDistance) {
		target = _target;
		minAcceptableDistance = _minAcceptableDistance;
	}

	public override void Update(PlayerModel _model, Rigidbody _rigidbody) {
		if (!isFinished) {
			Vector3 targetForward = target - _model.transform.position;
			targetForward.y = 0;

			if (targetForward.magnitude <= minAcceptableDistance) {
				isFinished = true;
				return;
			}

			targetForward.Normalize();
			_model.transform.forward = Vector3.MoveTowards(_model.transform.forward, targetForward, _model.rotationCorrectionSpeed * Time.deltaTime);
			_model.activeInputMagnitude = 1.0f;

			_rigidbody.velocity = _model.transform.forward * _model.movementSpeed;
		}
	}
}
