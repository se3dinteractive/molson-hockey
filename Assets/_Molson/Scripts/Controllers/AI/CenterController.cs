﻿using UnityEngine;
using System.Collections;

public class CenterController : MonoBehaviour, IPuckEventListenerInterface {

	enum ControllerState {
		NONE,
		WAIT_FOR_ONSIDE,
		HEAD_TO_NET,
		HEAD_TO_PUCK,
		DEFEND_SPOT
	};

	public float netDistance = 10.0f;
	public float blueLineDistance = 25.0f;

	public float minWaitTime = 0.1f;
	public float maxWaitTime = 0.5f;

	public float minShootDistance = 15.0f;
	public float maxShootDistance = 30.0f;

	public float reflexDelayTime = 0.4f;

	AIController aiController;
	RinkModel rinkModel;
	PlayerModel playerModel;
	PlayerController playerController;
	RulesEnforcer rulesEnforcer;
	IceZoneManager iceZoneMgr;

	int curTeamWithPuck;

	bool hasPuck = false;

	ControllerState state;

	// Use this for initialization
	void Start() {
		FindObjectOfType<PuckEventManager>().AddListener(this);

		aiController = GetComponent<AIController>();
		playerModel = GetComponent<PlayerModel>();
		playerController = GetComponent<PlayerController>();
		rinkModel = FindObjectOfType<RinkModel>();
		rulesEnforcer = FindObjectOfType<RulesEnforcer>();
		iceZoneMgr = FindObjectOfType<IceZoneManager>();

		curTeamWithPuck = -1;

		state = ControllerState.NONE;
	}

	// Update is called once per frame
	void Update() {
		if (aiController.enabled) {
			if (!hasPuck && playerModel.puckParent.childCount > 0) {
				curTeamWithPuck = playerModel.teamLayer;
				hasPuck = true;
			} else if (hasPuck && playerModel.puckParent.childCount <= 0) {
				state = ControllerState.NONE;
				hasPuck = false;
			}

			if (curTeamWithPuck == -1) {
				HeadToPuck();
			} else if (curTeamWithPuck == playerModel.teamLayer) {
				MakeOffensiveDecision();
			} else {
				MakeDefensiveDecision();
			}
		}
	}

	public void WaitForOnside() {
		if (state != ControllerState.WAIT_FOR_ONSIDE) {
			state = ControllerState.WAIT_FOR_ONSIDE;

			aiController.ClearAllActions();
			aiController.QueueAction(new MoveToPoint(rinkModel.GetAttackingBlueLinePosition(playerModel.teamLayer, 0.5f), 2.0f));
		}
	}

	public void HeadToNet() {
		if (state != ControllerState.HEAD_TO_NET) {
			state = ControllerState.HEAD_TO_NET;

			aiController.ClearAllActions();
			aiController.QueueAction(new MoveToPoint(rinkModel.GetAttackingNetPosition(playerModel.teamLayer), netDistance));
			aiController.QueueAction(new WaitAtCurrentPos(Random.Range(minWaitTime, maxWaitTime)));
			aiController.QueueAction(new MoveToPoint(rinkModel.GetAttackingBlueLinePosition(playerModel.teamLayer, 0.5f), blueLineDistance));
			aiController.QueueAction(new WaitAtCurrentPos(Random.Range(minWaitTime, maxWaitTime)));
			aiController.QueueAction(new CallBehaviourAction(this.ClearState));
			aiController.QueueAction(new CallBehaviourAction(this.HeadToNet));
		}
	}

	public void ClearState() {
		state = ControllerState.NONE;
	}

	public void HeadToPuck() {
		if (state != ControllerState.HEAD_TO_PUCK) {
			state = ControllerState.HEAD_TO_PUCK;

			aiController.ClearAllActions();
			aiController.QueueAction(new FollowObject(aiController.puckController.transform));
		}
	}

	public void DefendSpot() {
		if (state != ControllerState.DEFEND_SPOT) {
			state = ControllerState.DEFEND_SPOT;

			Vector3 targetPos = rinkModel.GetDefendingBlueLinePosition(playerModel.teamLayer, 0.5f) + rinkModel.GetDefendingDirection(playerModel.teamLayer) * 5.0f;
			targetPos.y = transform.position.y;

			aiController.ClearAllActions();
			aiController.QueueAction(new MoveToPoint(targetPos, 1.0f));
			aiController.QueueAction(new TrackPuckAction(aiController.puckController.transform, 5.0f, targetPos));
		}
	}

	void MakeOffensiveDecision() {
		if (hasPuck) {
			if (rulesEnforcer.IsInOffensiveZone(playerModel.teamLayer)) {
				float netDistance = (rinkModel.GetAttackingNetPosition(playerModel.teamLayer) - transform.position).magnitude;

				bool getRidOfPuck = Random.Range(0.0f, 1.0f) >= (netDistance - minShootDistance) / (maxShootDistance - minShootDistance);
				if (getRidOfPuck) {
					bool shoot = Random.Range(0, 5) % 5 == 0;
					if (!shoot) {
						playerController.StartShootWindUp();
						playerController.ReleaseShot();
					} else {
						playerController.ReleasePass();
					}
					hasPuck = false;
					curTeamWithPuck = -1;
				}
				/*} else if (iceZoneMgr.GetAwayPlayerInOffensiveZoneCount() > 0) {
					WaitForOnside();*/
			} else {
				HeadToNet();
			}
		} else {
			HeadToNet();
		}
		/*} else if (rulesEnforcer.IsInOffensiveZone(playerModel.teamLayer)) {
			if (state != ControllerState.HEAD_TO_NET) {
				HeadToNet();
			}
		} else {
			if (state != ControllerState.WAIT_FOR_ONSIDE) {
				WaitForOnside();
			}
		}*/
	}

	void MakeDefensiveDecision() {
		if (state != ControllerState.DEFEND_SPOT) {
			DefendSpot();
		}
	}

	public void OnZoneEnter(PuckEventManager.IceZone _zone) {
		/*if (curTeamWithPuck == -1) {
			HeadToPuck();
		} else if (curTeamWithPuck == playerModel.teamLayer) {
			MakeOffensiveDecision();
		} else {
			MakeDefensiveDecision();
		}*/
	}

	public void OnZoneExit(PuckEventManager.IceZone _zone) {

	}

	public void OnClaimed(int _teamLayer) {
		curTeamWithPuck = _teamLayer;
		if (curTeamWithPuck == -1) {
			HeadToPuck();
		} else if (curTeamWithPuck == playerModel.teamLayer) {
			MakeOffensiveDecision();
		} else {
			MakeDefensiveDecision();
		}
	}

	public void OnShot(int _teamLayer) {
		curTeamWithPuck = -1;
	}

	public void OnReset() {
		curTeamWithPuck = -1;
		aiController.ClearAllActions();
		state = ControllerState.NONE;
	}
}
