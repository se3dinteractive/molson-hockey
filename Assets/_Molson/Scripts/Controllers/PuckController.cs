﻿using UnityEngine;
using System.Collections;

public class PuckController : MonoBehaviour {

	public float velocityLoss = 0.98f;
	public float minimumPossessionTime = 0.4f;
	public GameObject highlight;

	Transform baseParent;
	bool isClaimed = false;
	Rigidbody myRigidbody;

	NetworkView myNetworkView;

	float elapsedPossessionTime = 0.0f;

	// Use this for initialization
	void Start() {
		baseParent = transform.parent;
		myRigidbody = GetComponent<Rigidbody>();
		myNetworkView = GetComponent<NetworkView>();
	}

	void Update() {
		elapsedPossessionTime += Time.deltaTime;
	}

	// Update is called once per frame
	void FixedUpdate() {
		if (!isClaimed && myRigidbody.velocity != Vector3.zero) {
			myRigidbody.velocity *= velocityLoss;
			if (myRigidbody.velocity.magnitude < 0.05f) myRigidbody.velocity = Vector3.zero;
		}
	}

	public void Free(Transform _caller) {
		if (transform.parent == _caller) {
			ForceFree();
		}
	}

	public void ForceFree() {
		if (myRigidbody == null) {
			myRigidbody = GetComponent<Rigidbody>();
		}

		transform.parent = null;
		myRigidbody.isKinematic = false;
		isClaimed = false;
		highlight.SetActive(true);
		elapsedPossessionTime = 0;
	}

	public void Claim(Transform newParent) {
		if (newParent != null) {
			transform.parent = newParent;
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;
			myRigidbody.isKinematic = true;
			isClaimed = true;
			highlight.SetActive(false);

			elapsedPossessionTime = 0.0f;
		}
	}

	public void Shoot(Vector3 velocity, Transform _caller) {
		Free(_caller);

		myRigidbody.velocity = velocity;
	}

	public int GetParentLayer() {
		return (transform.parent == baseParent) ? -1 : transform.parent.gameObject.layer;
	}

	void OnSerializeNetworkView(BitStream _stream, NetworkMessageInfo _info) {
		Vector3 syncPos = Vector3.zero;
		Vector3 syncVelo = Vector3.zero;

		if (_stream.isWriting) {
			syncPos = transform.position;
			syncVelo = myRigidbody.velocity;

			_stream.Serialize(ref syncPos);
			_stream.Serialize(ref syncVelo);
		} else {
			_stream.Serialize(ref syncPos);
			_stream.Serialize(ref syncVelo);

			myRigidbody.position = syncPos;
		}
	}
}
