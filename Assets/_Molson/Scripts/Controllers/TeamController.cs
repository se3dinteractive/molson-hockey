﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class TeamController : MonoBehaviour {

	public PlayerSceneObjectInfo playerSceneInfo;
	//public Material playerMaterial;
	public Color playerHighlightColour;
	public int numPlayers = 4;
	public GameObject onlinePlayerPrefab;

	public GameObject offlinePlayerPrefab;

	GameObject[] players;
	PlayerView[] playerViews;
	PlayerController[] playerControllers;
	PlayerModel[] playerModels;
	AIController[] aiControllers;

	int activePlayer;

	bool showClosestPlayer = false;
	bool switchToClosestPlayer = false;
	bool isInitialized = false;

	private PuckController puckController;
	private RinkModel rinkModel;

	// Use this for initialization
	void Start() {
		
	}

	public void Init(bool isOnline) {
		if (!isInitialized) {
			rinkModel = FindObjectOfType<RinkModel>();
			activePlayer = -1;
			players = new GameObject[transform.childCount];
			playerViews = new PlayerView[transform.childCount];
			playerModels = new PlayerModel[transform.childCount];
			playerControllers = new PlayerController[transform.childCount];
			aiControllers = new AIController[transform.childCount];

			for (int i = 0; i < transform.childCount; i++) {
				players[i] = transform.GetChild(i).gameObject;
				playerSceneInfo.CopyInfoToPlayer(players[i], (gameObject.layer == Config.HOME_TEAM_LAYER));
				playerViews[i] = players[i].GetComponent<PlayerView>();
				playerViews[i].playerHighlight.GetComponent<SpriteRenderer>().color = playerHighlightColour;
				playerModels[i] = players[i].GetComponent<PlayerModel>();
				playerModels[i].TeamID = i;
				playerControllers[i] = players[i].GetComponent<PlayerController>();
				aiControllers[i] = players[i].GetComponent<AIController>();
				if (isOnline) {
					aiControllers[i].enabled = (Network.isServer && gameObject.layer == Config.HOME_TEAM_LAYER) || (Network.isClient && gameObject.layer == Config.AWAY_TEAM_LAYER);
				}

				//playerModels[i].layerChild.GetComponent<MeshRenderer>().material = playerMaterial;
				playerModels[i].layerChild.layer = gameObject.layer;
				playerModels[i].teamLayer = gameObject.layer;
				playerModels[i].puckParent.gameObject.layer = gameObject.layer;
				playerControllers[i].Initialize();

			}

			puckController = FindObjectOfType<PuckController>();
			isInitialized = true;
		}
	}

	public void SetActivePlayerEnabled(bool _enabled) {
		if (activePlayer != -1 && playerControllers[activePlayer]) {
			playerControllers[activePlayer].enabled = _enabled;
		}
	}

	public PlayerController[] GetPlayerControllers() {
		return playerControllers;
	}

	public PlayerModel[] GetPlayerModels() {
		return playerModels;
	}

	// Update is called once per frame
	void Update() {
		if (isInitialized) {
			if (switchToClosestPlayer && activePlayer != -1) {
				SetActivePlayer(GetClosestPlayerModel(puckController.gameObject, false).TeamID);

				showClosestPlayer = switchToClosestPlayer = false;
			}

			if (showClosestPlayer) {
				ResetAllHighlights();

				if (activePlayer != -1) {
					GetClosestPlayerView(players[activePlayer], true).EnablePlayerHighlight();
				}
			}

			if (activePlayer != -1) {
				ShowActivePlayer();
				if (playerControllers[activePlayer].HasPuck()) {
					playerViews[activePlayer].ShowPuckControls();
				} else {
					playerViews[activePlayer].ShowNoPuckControls();
				}
			}
		}
	}

	public void SetActivePlayer(int _newPlayer) {
		if (activePlayer != -1) {
			//playerControllers[activePlayer].enabled = false;
			//aiControllers[activePlayer].enabled = true;
			playerControllers[activePlayer].SetIsPlayerControlled(false);
		}

		activePlayer = _newPlayer;
		if (activePlayer != -1) {
			//aiControllers[activePlayer].enabled = false;
			//playerControllers[activePlayer].enabled = true;
			playerControllers[activePlayer].SetIsPlayerControlled(true);
			//gameCamera.followTarget = players[activePlayer].transform;
		}

		ResetAllHighlights();
		ShowActivePlayer();
	}

	public void MakeCenterActive() {
		if ((Config.isOnline && (Network.isServer && gameObject.layer == Config.HOME_TEAM_LAYER || Network.isClient && gameObject.layer == Config.AWAY_TEAM_LAYER)) || (!Config.isOnline && gameObject.layer == Config.HOME_TEAM_LAYER)) {
			for (int i=0;i<playerModels.Length;i++) {
				if (playerModels[i].role == PlayerModel.PlayerRole.CENTER) {
					SetActivePlayer(i);
					break;
				}
			}
		}
	}

	public int GetActivePlayerIndex() {
		return activePlayer;
	}

	public int GetClosestPlayerIndex(GameObject _inquisitor, bool _onlyForward) {
		int closestPlayer = -1;

		float closestDistance = -1;
		float curDistance = 0;
		Vector3 curDiff;
		bool isBehind = true;
		float dot = 0;

		for (int i = 0; i < players.Length; i++) {
			curDiff = players[i].transform.position - _inquisitor.transform.position;
			curDistance = curDiff.magnitude;

			dot = Vector3.Dot(curDiff, rinkModel.GetAttackingNetPosition(gameObject.layer) - _inquisitor.transform.position);
			if (players[i] != _inquisitor && (closestDistance == -1 || curDistance < closestDistance) && ((_onlyForward && (isBehind || !isBehind && dot >= 0)) || !_onlyForward)) {
				closestDistance = curDistance;
				closestPlayer = i;
				isBehind = (dot < 0);
			}
		}

		return closestPlayer;
	}

	public GameObject GetClosestPlayer(GameObject _inquisitor, bool _onlyForward) {
		int index = GetClosestPlayerIndex(_inquisitor, _onlyForward);
		return (index >= 0) ? players[index] : null;
	}

	public PlayerView GetClosestPlayerView(GameObject _inquisitor, bool _onlyForward) {
		int index = GetClosestPlayerIndex(_inquisitor, _onlyForward);
		return (index >= 0) ? playerViews[index] : null;
	}

	public PlayerModel GetClosestPlayerModel(GameObject _inquisitor, bool _onlyForward) {
		int index = GetClosestPlayerIndex(_inquisitor, _onlyForward);
		return (index >= 0) ? playerModels[index] : null;
	}

	public PlayerController GetClosestPlayerController(GameObject _inquisitor, bool _onlyForward) {
		int index = GetClosestPlayerIndex(_inquisitor, _onlyForward);
		return (index >= 0) ? playerControllers[index] : null;
	}

	public void ResetAllHighlights() {
		for (int i = 0; i < playerViews.Length; i++) {
			if (playerViews[i]) {
				playerViews[i].DisablePlayerHighlight();
			}
		}
	}

	public void ResetAllAI() {
		for (int i = 0; i < aiControllers.Length; i++) {
			aiControllers[i].ClearAllActions();
		}
	}

	public void ShowClosestPlayer() {
		showClosestPlayer = true;
	}

	public void SwitchToClosestPlayer() {
		switchToClosestPlayer = true;
	}

	public void ShowActivePlayer() {
		if (activePlayer != -1 && playerViews[activePlayer]) {
			playerViews[activePlayer].EnablePlayerHighlight();
		}
	}

	public void StopShowingClosestPlayer() {
		ResetAllHighlights();
		showClosestPlayer = false;
		ShowActivePlayer();
	}

	public void ActivePlayerWindUp() {
		if (activePlayer != -1) {
			playerControllers[activePlayer].StartShootWindUp();
		}
	}

	public void ActivePlayerShoot() {
		if (activePlayer != -1) {
			playerControllers[activePlayer].ReleaseShot();
		}
	}

	public void ActivePlayerCancelShot() {
		if (activePlayer != -1) {
			playerControllers[activePlayer].CancelShot();
		}
	}

	public void ActivePlayerPass() {
		if (activePlayer != -1) {
			playerControllers[activePlayer].ReleasePass();
		}
	}

	public void ActivePlayerThrowHit() {
		if (activePlayer != -1) {
			playerControllers[activePlayer].StartHit();
		}
	}

	public void MarkTeamAsNetworkPlayers() {
		for (int i = 0; i < players.Length; i++) {
			playerControllers[i].enabled = false;
			//aiControllers[i].enabled = false;
		}
	}

	public void SpawnPlayers(bool isOnline) {
		GameObject player = null;
		for (int i = 0; i < numPlayers; i++) {
			RinkModel rinkModel = FindObjectOfType<RinkModel>();

			if (isOnline) {
				player = (GameObject)Network.Instantiate(onlinePlayerPrefab, onlinePlayerPrefab.transform.position, onlinePlayerPrefab.transform.rotation, 0);
			} else {
				player = (GameObject)Object.Instantiate(offlinePlayerPrefab, offlinePlayerPrefab.transform.position, offlinePlayerPrefab.transform.rotation);
			}

			PlayerModel playerModel = player.GetComponent<PlayerModel>();
			playerModel.teamLayer = gameObject.layer;
			playerModel.layerChild.layer = gameObject.layer;

			player.transform.parent = transform;
			
			if (i == numPlayers - 1) {
				Vector3 goalPos = rinkModel.GetDefendingNetPosition(gameObject.layer) - rinkModel.GetDefendingDirection(gameObject.layer) * 2.0f;
				goalPos.y = offlinePlayerPrefab.transform.position.y;
				player.transform.position = goalPos;
				
				player.name = "Goalie";
				playerModel.role = PlayerModel.PlayerRole.GOALIE;
				player.AddComponent<GoalieController>();
				player.GetComponent<PlayerController>().SetIsGoalie(true);
			} else if (i == 0) {
				player.name = "Center";
				playerModel.role = PlayerModel.PlayerRole.CENTER;
				player.AddComponent<CenterController>();
			} else if (i == 1) {
				player.name = "Right Wing";
				playerModel.role = PlayerModel.PlayerRole.RIGHT_WING;
				player.AddComponent<WingerController>();
			} else if (i == 2) {
				player.name = "Left Wing";
				playerModel.role = PlayerModel.PlayerRole.LEFT_WING;
				player.AddComponent<WingerController>();
			} else if (i == 3) {
				
			} else if (i == 4) {
				
			}
		}
	}
}
