﻿using UnityEngine;
using System.Collections;

public class PlayerPuckPickup : MonoBehaviour {

	public PlayerController playerController;

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {

	}

	void OnTriggerEnter(Collider _collider) {
		if (_collider.gameObject.layer == 9) {
			playerController.ForceTrigger(_collider);
		}
	}
}
