﻿using UnityEngine;
using System.Collections;

public class ExactFollow : MonoBehaviour {

	public Transform target;

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {
		if (target == null) {
			GameObject go = GameObject.Find("Puck(Clone)");
			if (go != null) {
				target = go.transform;
			}
		}
		if (target != null) {
			transform.position = target.position;
		}
	}
}
