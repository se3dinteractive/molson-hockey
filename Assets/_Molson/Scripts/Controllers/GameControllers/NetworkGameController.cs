﻿using UnityEngine;
using System.Collections;

public class NetworkGameController : GameController {

	private NetworkView myNetworkView;
	private GameLobbyManager gameLobbyMgr;

	private int readyToSpawnAckCount = 0;
	private int playersSpawnedAckCount = 0;
	private int readyToStartGameAckCount = 0;

	private bool waitForOtherTeamPlayers;
	private bool waitForPuck;

	void Start() {
		base.Init();
		gameLobbyMgr = FindObjectOfType<GameLobbyManager>();
		myNetworkView = GetComponent<NetworkView>();
	}

	void Update() {
		if (isCelebratingGoal) {
			elapsedGoalTime += Time.deltaTime;
			if (elapsedGoalTime >= goalCelebrationDuration) {
				isCelebratingGoal = false;
				refDispCont.Hide();
				gameTimer.ResumeGame();
				if (Network.isServer) {
					CallRPC("RunFaceoff", RPCMode.All, 0);
				}
			}
		}

		if (waitForPuck) {
			puck = GameObject.Find("Puck(Clone)");
			if (puck != null) {
				rulesEnforcer = FindObjectOfType<RulesEnforcer>();
				rulesEnforcer.enabled = true;
				rulesEnforcer.Init(puck);
				CallRPC("Acknowledge", RPCMode.All, "readyToSpawn");
				waitForPuck = false;
				FindObjectOfType<CameraFollow>().followTarget = puck.transform;
			}
		}

		if (waitForOtherTeamPlayers) {
			if ((Network.isServer && awayTeamController.transform.childCount == awayTeamController.numPlayers) || (Network.isClient && homeTeamController.transform.childCount == homeTeamController.numPlayers)) {
				CallRPC("Acknowledge", RPCMode.All, "playersSpawned");
				waitForOtherTeamPlayers = false;
			}
		}
	}

	public override void SetUsername(string _name) {
		CallRPC("DisplayUsername", RPCMode.All, _name);
	}

	public override void StartGame() {
		CallRPC("PrepareToSpawn", RPCMode.All);
	}

	public override void ScoreGoal(int _teamLayer) {
		CallRPC("ReportGoal", RPCMode.All, _teamLayer);
	}

	public override void StartFaceoff(int _target) {
		CallRPC("RunFaceoff", RPCMode.All, _target);
	}

	void CallRPC(string _name, RPCMode _mode, params object[] _args) {
		myNetworkView.RPC(_name, _mode, _args);
	}

	[RPC]
	void DisplayUsername(string _name) {
		if (gameLobbyMgr == null) {
			gameLobbyMgr = FindObjectOfType<GameLobbyManager>();
		}
		gameLobbyMgr.DisplayPlayer(_name);
	}

	[RPC]
	void PrepareToSpawn() {
		uiMgr.ChangeStateTo("HUD");
		if (Network.isServer) {
			puck = (GameObject)Network.Instantiate(puckPrefab, puckPrefab.transform.position, puckPrefab.transform.rotation, 0);
		}
		waitForPuck = true;
	}

	[RPC]
	void CreateNetworkObjects() {
		if (Network.isServer) {
			homeTeamController.SpawnPlayers(true);
		} else {
			awayTeamController.SpawnPlayers(true);
		}

		waitForOtherTeamPlayers = true;
	}

	[RPC]
	void SetupGame() {
		homeTeamController.Init(true);
		awayTeamController.Init(true);

		if (Network.isServer) {
			homeTeamController.SetActivePlayer(0);
		} else {
			awayTeamController.SetActivePlayer(0);
		}

		CallRPC("Acknowledge", RPCMode.All, "readyToStartGame");
	}

	[RPC]
	void RunFaceoff(int _target) {
		rulesEnforcer.faceoffs[_target].SetupFaceoff();
	}

	[RPC]
	void StartFirstFaceoff(int _target) {
		gameTimer.StartGame();
		RunFaceoff(_target);
	}

	[RPC]
	void ReportGoal(int _netLayer) {
		if (refDispCont == null) refDispCont = FindObjectOfType<RefDisplayController>();
		gameTimer.ScoreGoal(_netLayer);
		isCelebratingGoal = true;
		elapsedGoalTime = 0;
		refDispCont.ShowGoal();
		gameTimer.PauseGame();
	}

	[RPC]
	void Acknowledge(string _ack) {
		if (_ack == "playersSpawned") {
			playersSpawnedAckCount++;
			if (Network.isServer && playersSpawnedAckCount == 2) {
				playersSpawnedAckCount = 0;
				CallRPC("SetupGame", RPCMode.All);
			}
		} else if (_ack == "readyToStartGame") {
			readyToStartGameAckCount++;
			if (Network.isServer && readyToStartGameAckCount == 2) {
				readyToStartGameAckCount = 0;
				CallRPC("StartFirstFaceoff", RPCMode.All, 0);
			}
		} else if (_ack == "readyToSpawn") {
			readyToSpawnAckCount++;
			if (Network.isServer && readyToSpawnAckCount == 2) {
				readyToSpawnAckCount = 0;
				CallRPC("CreateNetworkObjects", RPCMode.All);
			}
		}
	}
}
