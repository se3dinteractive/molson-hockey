﻿using UnityEngine;
using System.Collections;

public abstract class GameController : MonoBehaviour {

	public GameObject puck;
	public GameObject puckPrefab;

	public TeamController homeTeamController;
	public TeamController awayTeamController;

	public float goalCelebrationDuration;

	protected RulesEnforcer rulesEnforcer;
	protected UIStateController uiMgr;
	protected GameTimer gameTimer;
	protected RefDisplayController refDispCont;

	protected float elapsedGoalTime;
	protected bool isCelebratingGoal;

	public virtual void Init() {
		uiMgr = FindObjectOfType<UIStateController>();
		rulesEnforcer = FindObjectOfType<RulesEnforcer>();
		gameTimer = FindObjectOfType<GameTimer>();
		refDispCont = FindObjectOfType<RefDisplayController>();

		puck = null;
	}

	public virtual void SetUsername(string _name) {

	}

	public abstract void StartGame();

	public abstract void ScoreGoal(int _teamLayer);

	public abstract void StartFaceoff(int _target);
}
