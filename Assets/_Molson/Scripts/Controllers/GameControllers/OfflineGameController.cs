﻿using UnityEngine;
using System.Collections;

public class OfflineGameController : GameController {

	private bool frameSkip;
	private CameraFollow camFollow;

	// Use this for initialization
	void Start() {
		
	}

	// Update is called once per frame
	void Update() {
		if (frameSkip) {
			gameTimer.StartGame();
			frameSkip = false;
		}

		if (isCelebratingGoal) {
			elapsedGoalTime += Time.deltaTime;
			if (elapsedGoalTime >= goalCelebrationDuration) {
				isCelebratingGoal = false;
				refDispCont.Hide();
				gameTimer.ResumeGame();
				StartFaceoff(0);
			}
		}
	}

	public override void StartGame() {
		base.Init();

		//SeedAnalytics.GetInstance().StartTimer("game_duration");

		Config.isOnline = false;
		uiMgr.ChangeStateTo("HUD");

		puck = (GameObject)Object.Instantiate(puckPrefab, puckPrefab.transform.position, puckPrefab.transform.rotation);
		camFollow = FindObjectOfType<CameraFollow>();
		if (camFollow) {
			camFollow.followTarget = puck.transform;
		}
		rulesEnforcer = FindObjectOfType<RulesEnforcer>();
		rulesEnforcer.Init(puck);

		homeTeamController.SpawnPlayers(false);
		awayTeamController.SpawnPlayers(false);

		homeTeamController.Init(false);
		awayTeamController.Init(false);

		homeTeamController.SetActivePlayer(0);

		frameSkip = true;
	}

	public override void ScoreGoal(int _teamLayer) {
		if (_teamLayer == Config.HOME_TEAM_LAYER) {
			SeedAnalytics.GetInstance().ReportEvent("scored_on");
		} else if (_teamLayer == Config.AWAY_TEAM_LAYER) {
			SeedAnalytics.GetInstance().ReportEvent("scored");
		}
		gameTimer.PauseGame();
		if (refDispCont == null) refDispCont = FindObjectOfType<RefDisplayController>();
		gameTimer.ScoreGoal(_teamLayer);
		isCelebratingGoal = true;
		elapsedGoalTime = 0;
		refDispCont.ShowGoal();
	}

	public override void StartFaceoff(int _target) {
		rulesEnforcer.faceoffs[(int)_target].SetupFaceoff();
		camFollow.JumpToPos(rulesEnforcer.faceoffs[(int)_target].transform.position);
	}
}
