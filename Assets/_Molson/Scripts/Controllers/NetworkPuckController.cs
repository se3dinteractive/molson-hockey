﻿using UnityEngine;
using System.Collections;

public class NetworkPuckController : MonoBehaviour {

	public float positionOffsetThreshold;
	public float rotationOffsetThreshold;
	public float velocityOffsetThreshold;
	public float positionLerpSpeed;
	public float rotationSlerpDuration;

	private NetworkView myNetworkView;
	private Rigidbody myRigidbody;

	private Vector3 activePos;
	private Quaternion activeRotation;

	private Vector3 drPos;
	private Vector3 drVelocity;
	private Quaternion drRotation;

	private float timeSinceLastUpdate;

	private Vector3 prevNetworkPos;
	private Quaternion prevNetworkRotation;

	// Use this for initialization
	void Start() {
		myNetworkView = GetComponent<NetworkView>();
		myRigidbody = GetComponent<Rigidbody>();

		if (myNetworkView.isMine) {
			myNetworkView.RPC("UpdateNetworkTransform", RPCMode.All, myRigidbody.position, myRigidbody.velocity, myRigidbody.rotation);
		}
	}

	// Update is called once per frame
	void Update() {
		timeSinceLastUpdate += Time.deltaTime;

		if (drPos != prevNetworkPos) {
			drPos = Vector3.MoveTowards(drPos, prevNetworkPos, positionLerpSpeed * Time.deltaTime);
		}

		if (drRotation != prevNetworkRotation) {
			drRotation = Quaternion.Slerp(drRotation, prevNetworkRotation, Mathf.Min(timeSinceLastUpdate / rotationSlerpDuration, 1.0f));
		}

		activePos = drPos + drVelocity * timeSinceLastUpdate;
		activeRotation = drRotation;

		if (myNetworkView.isMine) {
			if ((activePos - myRigidbody.position).magnitude >= positionOffsetThreshold ||
				Quaternion.Angle(activeRotation, myRigidbody.rotation) >= rotationOffsetThreshold ||
				(drVelocity - myRigidbody.velocity).magnitude >= velocityOffsetThreshold) {
				myNetworkView.RPC("UpdateNetworkTransform", RPCMode.All, myRigidbody.position, myRigidbody.velocity, myRigidbody.rotation);
			}
			drVelocity = myRigidbody.velocity;
		} else {
			transform.position = activePos;
			transform.rotation = activeRotation;
		}
	}

	[RPC]
	public void UpdateNetworkTransform(Vector3 _position, Vector3 _velocity, Quaternion _rotation) {
		prevNetworkPos = _position;
		drVelocity = _velocity;
		prevNetworkRotation = _rotation;

		timeSinceLastUpdate = 0.0f;
	}
}
