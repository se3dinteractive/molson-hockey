﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerSceneObjectInfo : MonoBehaviour {

	public GameObject shootButton;
	public GameObject passButton;
	public GameObject hitButton;
	public GameObject switchPlayerButton;
	public Image powerBar;
	public Image powerBarFill;

	public JoystickController joystick;
	public Transform awayTeamNet;
	public Transform homeTeamNet;

	public PuckController puckController;

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {

	}

	public void CopyInfoToPlayer(GameObject _player, bool _isHomeTeam) {
		PlayerView view = _player.GetComponent<PlayerView>();
		view.shootButton = shootButton;
		view.passButton = passButton;
		view.hitButton = hitButton;
		view.switchPlayerButton = switchPlayerButton;
		view.powerBar = powerBar;
		view.powerBarFill = powerBarFill;

		PlayerInputController controller = _player.GetComponent<PlayerInputController>();
		controller.joystick = joystick;

		puckController = FindObjectOfType<PuckController>();

		_player.GetComponent<AIController>().puckController = puckController;
	}
}
