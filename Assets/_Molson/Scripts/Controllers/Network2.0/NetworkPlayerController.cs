﻿using UnityEngine;
using System.Collections;

public class NetworkPlayerController : MonoBehaviour {

	private PlayerModel playerModel;
	private Rigidbody myRigidbody;
	private NetworkView myNetworkView;

	private Vector3 targetPosition;
	private Quaternion targetForward;

	void Start() {
		playerModel = GetComponent<PlayerModel>();
		myRigidbody = GetComponent<Rigidbody>();
		myNetworkView = GetComponent<NetworkView>();

		targetPosition = transform.position;
		targetForward = transform.rotation;
	}

	void FixedUpdate() {
		if (!myNetworkView.isMine) {
			Synchronize();
		}
	}

	void OnSerializeNetworkView(BitStream _stream, NetworkMessageInfo _info) {
		Vector3 syncPos = Vector3.zero;
		Quaternion syncForward = Quaternion.identity;

		if (_stream.isWriting) {
			syncPos = transform.position;
			syncForward = transform.rotation;

			_stream.Serialize(ref syncPos);
			_stream.Serialize(ref syncForward);
		} else {
			_stream.Serialize(ref syncPos);
			_stream.Serialize(ref syncForward);

			targetPosition = syncPos;
			targetForward = syncForward;
		}
	}

	public void Synchronize() {
		transform.position = Vector3.MoveTowards(transform.position, targetPosition, playerModel.movementSpeed * Time.fixedDeltaTime);
		//transform.forward = Vector3.MoveTowards(transform.forward, targetForward, playerModel.rotationCorrectionSpeed * Time.fixedDeltaTime);
		transform.rotation = Quaternion.Slerp(transform.rotation, targetForward, playerModel.rotationCorrectionSpeed * Time.fixedDeltaTime);
		myRigidbody.velocity = Vector3.zero;
	}
}