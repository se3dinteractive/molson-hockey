﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public float followSpeed = 5.0f;
	public Transform followTarget;

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void FixedUpdate() {
		if (followTarget) {
			Vector3 targetPos = new Vector3(followTarget.position.x, transform.position.y, followTarget.position.z);
			transform.position = Vector3.MoveTowards(transform.position, targetPos, followSpeed * Time.deltaTime);
		}
	}

	public void JumpToPos(Vector3 _target) {
		transform.position = new Vector3(_target.x, transform.position.y, _target.z);
	}
}
