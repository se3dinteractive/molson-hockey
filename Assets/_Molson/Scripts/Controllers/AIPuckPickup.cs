﻿using UnityEngine;
using System.Collections;

public class AIPuckPickup : MonoBehaviour {

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {

	}

	void OnTriggerEnter(Collider _collider) {
		if (_collider.gameObject.layer == Config.PUCK_LAYER) {
			_collider.gameObject.GetComponent<PuckController>().Claim(transform);
		}
	}
}
