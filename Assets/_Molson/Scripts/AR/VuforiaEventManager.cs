﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Vuforia;

public class VuforiaEventManager : MonoBehaviour, ITrackableEventHandler {

	private List<IAREventInterface> arEventListeners;
	private TrackableBehaviour trackableBehaviour;

	// Use this for initialization
	void Start() {
		arEventListeners = new List<IAREventInterface>();
		trackableBehaviour = GetComponent<TrackableBehaviour>();
		if (trackableBehaviour != null) {
			trackableBehaviour.RegisterTrackableEventHandler(this);
		}
	}

	public void OnTrackableStateChanged(TrackableBehaviour.Status previousStats, TrackableBehaviour.Status newStatus) {
		if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {
			OnTrackingFound();
		} else {
			OnTrackingLost();
		}
	}

	public void RegisterAREventListener(IAREventInterface _listener) {
		arEventListeners.Add(_listener);
	}

	private void OnTrackingFound() {
		for (int i = 0; i < arEventListeners.Count; i++) {
			arEventListeners[i].OnMarkerFound();
		}
	}

	private void OnTrackingLost() {
		for (int i = 0; i < arEventListeners.Count; i++) {
			arEventListeners[i].OnMarkerLost();
		}
	}
}
