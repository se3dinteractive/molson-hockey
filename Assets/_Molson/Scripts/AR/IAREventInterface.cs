﻿using UnityEngine;
using System.Collections;

public interface IAREventInterface {

	void OnMarkerFound();

	void OnMarkerLost();
	
}
