﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UserManager : MonoBehaviour {

	public string username = "New Player";
	public int selectedTeam = 0;
	public UIStateController uiStateMgr;
	public GameObject userDisplayPanel;
	public Text userDisplayLabel;
	public Image userTeamDisplay;
	public SpriteRenderer centerIceSprite;
	public CreateTeamDisplay teamDisplay;

	// Use this for initialization
	void Start() {
		if (PlayerPrefs.HasKey("username")) {
			LoadSettings();
		} else {
			uiStateMgr.ChangeStateTo("Registration");
		}
	}

	// Update is called once per frame
	void Update() {

	}

	public void SubmitRegistration() {
		SaveSettings();
		LoadSettings();

		uiStateMgr.ChangeStateTo("StartScreen");
	}

	public void SaveSettings() {
		PlayerPrefs.SetString("username", username);
		PlayerPrefs.SetInt("selected_team", selectedTeam);

		PlayerPrefs.Save();
	}

	public void LoadSettings() {
		username = PlayerPrefs.GetString("username");
		selectedTeam = PlayerPrefs.GetInt("selected_team");

		userDisplayLabel.text = username;
		userTeamDisplay.sprite = teamDisplay.logoSprites[selectedTeam];
		userDisplayPanel.SetActive(true);

		teamDisplay.SetActiveLogoColour(selectedTeam);
		teamDisplay.MoveToTargetPosition();

		centerIceSprite.sprite = teamDisplay.logoSprites[selectedTeam];
	}

	public void UpdateUsername(string _newName) {
		username = _newName;
	}

#if UNITY_EDITOR
	[MenuItem("PlayerPrefs/Clear")]
	static void ClearPlayerPrefs() {
		PlayerPrefs.DeleteAll();
	}
#endif
}
