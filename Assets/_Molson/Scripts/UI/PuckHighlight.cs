﻿using UnityEngine;
using System.Collections;

public class PuckHighlight : MonoBehaviour {

	public float rotateSpeed = 1.0f;

	Vector3 eulerAngles;

	// Use this for initialization
	void Start() {
		eulerAngles = transform.eulerAngles;
	}

	// Update is called once per frame
	void LateUpdate() {
		eulerAngles.z += rotateSpeed;

		transform.rotation = Quaternion.Euler(eulerAngles);
	}
}
