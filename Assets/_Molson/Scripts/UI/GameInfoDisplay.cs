﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class GameInfoDisplay : MonoBehaviour {

	public Button joinButton;
	public Text gameName;
	public Text userName;
	public RectTransform myRect;

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {

	}

	public void SetupDisplay(Transform _parent, Vector2 _pos, UnityAction _call, string _userName, string _gameName) {
		transform.SetParent(_parent, false);
		myRect.localScale = new Vector3(1, 1, 1);
		myRect.anchoredPosition = _pos;

		joinButton.onClick.AddListener(_call);
		userName.text = _userName;
		gameName.text = _gameName;
	}
}
