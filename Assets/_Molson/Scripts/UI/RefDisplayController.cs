﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RefDisplayController : MonoBehaviour {

	public Image refDisplayImage;
	public Text refLabel;
	public Image refLabelBG;

	public Sprite icingSprite;
	public string icingText;
	public Sprite offsideSprite;
	public string offsideText;
	public Sprite outOfPlaySprite;
	public string outOfPlayText;
	public Sprite goalSprite;
	public string goalText;

	public AudioSource whistleSound;

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {

	}

	public void Hide() {
		refDisplayImage.gameObject.SetActive(false);
		refLabel.gameObject.SetActive(false);
		refLabelBG.gameObject.SetActive(false);
	}

	public void Show() {
		refDisplayImage.gameObject.SetActive(true);
		refLabelBG.gameObject.SetActive(true);
		refLabel.gameObject.SetActive(true);
	}

	public void ShowIcing() {
		whistleSound.Play();
		refDisplayImage.sprite = icingSprite;
		refLabel.text = icingText;
		Show();
	}

	public void ShowOffside() {
		whistleSound.Play();
		refDisplayImage.sprite = offsideSprite;
		refLabel.text = offsideText;
		Show();
	}

	public void ShowOutOfPlay() {
		whistleSound.Play();
		refDisplayImage.sprite = outOfPlaySprite;
		refLabel.text = outOfPlayText;
		Show();
	}

	public void ShowGoal() {
		whistleSound.Play();
		refDisplayImage.sprite = goalSprite;
		refLabel.text = goalText;
		Show();
	}

	public void ShowDelayedOffside() {
		refDisplayImage.sprite = offsideSprite;
		refLabel.text = "Delayed offside";
		Show();
	}

	public void ShowHeldPuck() {
		refDisplayImage.sprite = offsideSprite;
		refLabel.text = "Puck Frozen";
		Show();
	}
}
