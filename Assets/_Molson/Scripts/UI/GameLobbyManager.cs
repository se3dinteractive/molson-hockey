﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameLobbyManager : MonoBehaviour {

	public GameObject puckPrefab;
	public GameObject networkGameControllerPrefab;

	public Text hostPlayerNameLabel;
	public Text clientPlayerNameLabel;

	public GameObject clientGameLobbyButtons;
	public GameObject hostGameLobbyButtons;

	public TeamController homeTeamController;
	public TeamController awayTeamController;
	
	private GameDiscoveryManager gameDiscoveryMgr;
	private UserManager userMgr;

	private NetworkGameController gameController;

	bool isConnected = false;
	bool hasSentName = false;

	/** 
	 * 
	 * MONOBEHAVIOUR FUNCTIONS
	 * 
	 **/

	void Start() {
		gameDiscoveryMgr = FindObjectOfType<GameDiscoveryManager>();
		userMgr = FindObjectOfType<UserManager>();

		if (Network.isServer) {
			hostPlayerNameLabel.text = userMgr.username;
			gameController = (Network.Instantiate(networkGameControllerPrefab, Vector3.zero, Quaternion.identity, 0) as GameObject).GetComponent<NetworkGameController>();
			gameController.puckPrefab = puckPrefab;
		} else {
			clientPlayerNameLabel.text = userMgr.username;
		}
	}

	void Update() {
		if (isConnected && !hasSentName) {
			if (gameController == null) {
				gameController = FindObjectOfType<NetworkGameController>();
			}

			if (gameController != null) {
				gameController.SetUsername(userMgr.username);
				gameController.homeTeamController = homeTeamController;
				gameController.awayTeamController = awayTeamController;
			}

			hasSentName = true;
		}
	}

	void OnConnectedToServer() {
		OnConnectionStarted();
	}

	void OnPlayerConnected(NetworkPlayer _player) {
		OnConnectionStarted();
	}

	/**
	 * 
	 * CUSTOM FUNCTIONS
	 * 
	 **/

	void OnConnectionStarted() {
		gameDiscoveryMgr.CleanUp();

		isConnected = true;

		if (Network.isServer) {
			hostGameLobbyButtons.SetActive(true);
		} else {
			clientGameLobbyButtons.SetActive(true);
		}
	}

	public void DisplayPlayer(string _name) {
		if (Network.isServer) {
			clientPlayerNameLabel.text = _name;
		} else {
			hostPlayerNameLabel.text = _name;
		}
	}

	public void StartGame() {
		if (Network.isServer && gameController != null) {
			gameController.GetComponent<NetworkView>().RPC("PrepareToSpawn", RPCMode.All);
		}
	}
}
