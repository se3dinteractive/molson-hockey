﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ApplyGameSettings : MonoBehaviour {

	public GameTimer gameTimer;
	public Text periodLabel;
	public Text periodLengthLabel;

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {

	}

	public void ApplySettings() {
		gameTimer.numPeriods = int.Parse(periodLabel.text);
		gameTimer.periodLength = int.Parse(periodLengthLabel.text);
	}
}
