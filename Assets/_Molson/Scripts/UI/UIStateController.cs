﻿using UnityEngine;
using System.Collections;

public class UIStateController : MonoBehaviour {

	public string startState;

	public bool useAR = false;
	public GameObject[] toEnableOnGameStart;
	public GameObject[] toEnableOnARStart;
	public GameObject[] toEnableOnRegularStart;

	public GameObject background;

	GameObject activeState;

	// Use this for initialization
	void Start() {
		if (activeState == null) {
			ChangeStateTo(startState);
		}

		SetSceneObjectsActive(false);
	}

	// Update is called once per frame
	void Update() {

	}

	public void ChangeStateTo(string _stateName) {

		for (int i = 0; i < transform.childCount; i++) {
			if (transform.GetChild(i).gameObject.name == _stateName) {
				if (activeState != null) {
					SeedAnalytics.GetInstance().StopTimer(activeState.name);
					if (activeState.name == "HUD") {
						SetSceneObjectsActive(false);
					}
					activeState.SetActive(false);
				}

				activeState = transform.GetChild(i).gameObject;
				activeState.SetActive(true);
			}
		}

		if (activeState != null) {
			if (activeState.name == "HUD") {
				SetSceneObjectsActive(true);
			}
			SeedAnalytics.GetInstance().StartTimer(activeState.name);
		} else {
			Debug.LogError("No UI state with name: " + _stateName + " found!");
		}
	}

	public void SetSceneObjectsActive(bool _active) {
		for (int i = 0; i < toEnableOnGameStart.Length; i++) {
			toEnableOnGameStart[i].SetActive(_active);
		}

		if (useAR || !_active) {
			for (int i = 0; i < toEnableOnARStart.Length; i++) {
				toEnableOnARStart[i].SetActive(_active);
			}
		}

		if (!useAR || !_active) {
			for (int i = 0; i < toEnableOnRegularStart.Length; i++) {
				toEnableOnRegularStart[i].SetActive(_active);
			}
		}

		background.SetActive(!_active);
	}

	public void HideActiveState() {
		activeState.SetActive(false);
		activeState = null;
	}
}
