﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Log : MonoBehaviour {

	public Text label; 

	public static string output;

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {
		label.text = output;
	}
}
