﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ScrollRectSnap : MonoBehaviour {

	public int numPages;
	public int pageWidth;
	public RectTransform scrollObject;
	public ScrollRect scrollRect;
	public float animateSpeed = 5.0f;

	Vector3 targetPos;
	bool animateToTarget = false;

	void Start() {
		
	}

	void Update() {
		if (animateToTarget) {
			scrollObject.localPosition = Vector3.MoveTowards(scrollObject.localPosition, targetPos, animateSpeed * Time.deltaTime);
			if (scrollObject.localPosition == targetPos) {
				animateToTarget = false;
			}
		}	
	}

	public void OnPointerUp() {
		Vector3 pos = scrollObject.localPosition;
		targetPos = new Vector3(FindSnapPoint(pos.x) - 400.0f, 0, 0);

		scrollRect.velocity = Vector2.zero;
		animateToTarget = true;
	}

	float FindSnapPoint(float _pos) {
		
		float target = Mathf.Abs(_pos) - 400.0f + pageWidth / 2.0f;

		return -Mathf.FloorToInt(target / pageWidth) * pageWidth;
	}

}