﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class JoystickController : MonoBehaviour {

	public Image joystickDisplay;
	public float radius;
	public float resetSpeed;

	public Transform arCamera;
	public Transform arCam2;

	Vector2 curJoystickPos;
	Vector2 originalJoystickPos;
	Vector2 touchOrigin;
	Vector2 curTouchPos;
	bool isPressed = false;

	float speedMagnitude;
	Vector2 direction;

	// Use this for initialization
	void Start() {
		originalJoystickPos = joystickDisplay.transform.position;
		curJoystickPos = originalJoystickPos;
	}

	// Update is called once per frame
	void Update() {

		if (!isPressed && curJoystickPos != originalJoystickPos) {
			curJoystickPos = Vector2.MoveTowards(curJoystickPos, originalJoystickPos, resetSpeed * Time.deltaTime);
			joystickDisplay.transform.position = curJoystickPos;

			Vector2 offset = curJoystickPos - originalJoystickPos;
			speedMagnitude = offset.magnitude / radius;
			direction = offset.normalized;

			Vector3 cameraForward = arCamera.forward;
			if (Mathf.Abs(cameraForward.x) <= 0.05f && Mathf.Abs(cameraForward.z) <= 0.05f) {
				cameraForward = arCamera.up;
			}

			cameraForward.y = 0;
			cameraForward.Normalize();

			Vector3 startVector = new Vector3(0, 0, 1);

			float angle = Vector3.Angle(startVector, cameraForward);
			if (cameraForward.x < 0) {
				angle = -angle;
			}

			Vector3 newDir = Quaternion.AngleAxis(angle, Vector3.up) * Vector3.Normalize(new Vector3(direction.x, 0, direction.y));
			direction = new Vector2(newDir.x, newDir.z);
		}
	}

	public float GetSpeedMagnitude() {
		return speedMagnitude;
	}

	public Vector2 GetDirection() {
		return direction;
	}

	public void OnPointerDown(BaseEventData eventData) {
		if (!isPressed) {
			touchOrigin = ((PointerEventData)eventData).position;
			isPressed = true;
		}
	}

	public void OnPointerUp(BaseEventData eventData) {
		if (isPressed) {
			isPressed = false;
		}
	}

	public void OnMove(BaseEventData eventData) {
		if (isPressed) {
			curTouchPos = ((PointerEventData)eventData).position;

			curJoystickPos = curTouchPos - touchOrigin;
			speedMagnitude = curJoystickPos.magnitude;
			direction = curJoystickPos.normalized;
			if (speedMagnitude > radius) {
				curJoystickPos = direction * radius;
				speedMagnitude = radius;
			}
			speedMagnitude /= radius;
			curJoystickPos += originalJoystickPos;

			joystickDisplay.transform.position = curJoystickPos;

			Vector3 cameraForward = arCamera.forward;
			if (Mathf.Abs(cameraForward.x) <= 0.05f && Mathf.Abs(cameraForward.z) <= 0.05f) {
				cameraForward = arCamera.up;
			}

			cameraForward.y = 0;
			cameraForward.Normalize();

			Vector3 startVector = new Vector3(0, 0, 1);

			/*if (Input.deviceOrientation == DeviceOrientation.LandscapeRight) {
				startVector.z = -1;
			} else if (Input.deviceOrientation == DeviceOrientation.Portrait) {
				startVector.x = 1;
				startVector.z = 0;
			} else if (Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown) {
				startVector.x = -1;
				startVector.z = 0;
			}*/

			float angle = Vector3.Angle(startVector, cameraForward);
			if (cameraForward.x < 0) {
				angle = -angle;
			}

			Vector3 newDir = Quaternion.AngleAxis(angle, Vector3.up) * new Vector3(direction.x, 0, direction.y);
			direction =  new Vector2(newDir.x, newDir.z);
		}
	}
}
