﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CreateTeamDisplay : MonoBehaviour {

	public Sprite[] logoSprites;
	public float width = 126;
	public float margin = 100;
	public Color disabledColour;

	public int pageWidth;
	public ScrollRect scrollRect;
	public float animateSpeed = 5.0f;

	public UserManager userManager;

	Vector3 targetPos;
	bool animateToTarget = false;

	RectTransform myRect;
	Image[] images;
	int activeIndex = 0;

	// Use this for initialization
	void Start() {
		myRect = GetComponent<RectTransform>();

		float totalWidth = (logoSprites.Length) * width + 500;
		myRect.sizeDelta = new Vector2(totalWidth, 276);

		GameObject sprite;
		images = new Image[logoSprites.Length];
		for (int i = 0; i < logoSprites.Length; i++) {
			sprite = new GameObject();
			images[i] = sprite.AddComponent<Image>();
			images[i].sprite = logoSprites[i];
			sprite.transform.SetParent(transform, false);

			images[i].rectTransform.sizeDelta = new Vector2(256, 256);
			images[i].rectTransform.anchorMax = new Vector2(0, 0.5f);
			images[i].rectTransform.anchorMin = new Vector2(0, 0.5f);
			images[i].rectTransform.localPosition = new Vector3(400 + (i * width), 0, 0);
			images[i].color = disabledColour;
		}

		images[activeIndex].color = Color.white;	
	}

	public void SetActiveLogoColour(int _newLogo) {
		if (_newLogo != activeIndex) {
			if (images != null) {
				images[activeIndex].color = disabledColour;
				images[_newLogo].color = Color.white;
			}
			activeIndex = _newLogo;

			userManager.selectedTeam = activeIndex;
			userManager.SaveSettings();
			userManager.LoadSettings();
		}
	}

	public void MoveToTargetPosition() {
		if (myRect == null) {
			myRect = GetComponent<RectTransform>();
		}
		myRect.localPosition = new Vector3(-activeIndex * pageWidth - 400.0f, 0, 0);
	}

	void Update() {
		if (animateToTarget) {
			myRect.localPosition = Vector3.MoveTowards(myRect.localPosition, targetPos, animateSpeed * Time.deltaTime);
			if (myRect.localPosition == targetPos) {
				animateToTarget = false;
			}
		}
	}

	public void OnPointerUp() {
		Vector3 pos = myRect.localPosition;
		targetPos = new Vector3(FindSnapPoint(pos.x) - 400.0f, 0, 0);

		scrollRect.velocity = Vector2.zero;
		animateToTarget = true;
	}

	float FindSnapPoint(float _pos) {

		float target = Mathf.Abs(_pos) - 400.0f + pageWidth / 2.0f;

		int newIndex = Mathf.FloorToInt(target / pageWidth);
		SetActiveLogoColour(newIndex);

		return -activeIndex * pageWidth;
	}
}
