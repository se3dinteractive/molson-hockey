﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ARToggle : MonoBehaviour {

	public Text buttonLabel;

	UIStateController uiStateCont;

	// Use this for initialization
	void Start () {
		uiStateCont = FindObjectOfType<UIStateController>();
		if (PlayerPrefs.HasKey("USE_AR")) {
			uiStateCont.useAR = PlayerPrefs.GetInt("USE_AR") == 1;
		}

		if (uiStateCont.useAR) {
			buttonLabel.text = "Turn AR Off";
		} else {
			buttonLabel.text = "Turn AR On";
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ToggleAR() {
		uiStateCont.useAR = !uiStateCont.useAR;
		if (uiStateCont.useAR) {
			buttonLabel.text = "Turn AR Off";
		} else {
			buttonLabel.text = "Turn AR On";
		}

		PlayerPrefs.SetInt("USE_AR", (uiStateCont.useAR) ? 1 : 0);
		PlayerPrefs.Save();
	}
}
