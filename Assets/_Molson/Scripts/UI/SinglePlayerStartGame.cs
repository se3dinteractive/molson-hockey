﻿using UnityEngine;
using System.Collections;

public class SinglePlayerStartGame : MonoBehaviour {

	public GameObject offlineGameControllerPrefab;

	public TeamController homeTeamController;
	public TeamController awayTeamController;

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {

	}

	public void StartGame() {
		OfflineGameController gameCont = ((GameObject)Object.Instantiate(offlineGameControllerPrefab, Vector3.zero, Quaternion.identity)).GetComponent<OfflineGameController>();
		gameCont.homeTeamController = homeTeamController;
		gameCont.awayTeamController = awayTeamController;

		gameCont.StartGame();
	}
}
