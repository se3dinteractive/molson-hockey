﻿using UnityEngine;
using System.Collections;

public class StatsReporter : MonoBehaviour, SproutServerEventListener {

	private bool isServerReady = false;
	private bool sessionStarted = false;

	// Use this for initialization
	void Start() {
		FindObjectOfType<SproutServerManager>().AddEventListener(this);
	}

	// Update is called once per frame
	void Update() {
		if (isServerReady && !sessionStarted) {
			SeedAnalytics.GetInstance().StartSession();
			sessionStarted = true;
		}
	}

	void OnApplicationQuit() {
		if (isServerReady) {
			SeedAnalytics.GetInstance().StopSession();
		}
	}

	public void ReportButton(string _name) {
		SeedAnalytics.GetInstance().ReportEvent(_name);
	}

	/**
	 * SproutServerEventListener implementations.
	 * 
	 * */

	public void OnMessageServerStatusChange(ServerStatus _status) {

	}

	public void OnStatServerStatusChange(ServerStatus _status) {
		isServerReady = (_status == ServerStatus.AVAILABLE);
	}
}
