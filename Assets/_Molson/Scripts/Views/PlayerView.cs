﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerView : MonoBehaviour {

	public enum ShotType {
		SHOT,
		PASS
	};

	public GameObject shootButton;
	public GameObject passButton;
	public GameObject hitButton;
	public GameObject switchPlayerButton;

	public Image powerBar;
	public Image powerBarFill;
	public float maxPowerBarFillWidth = 220.0f;
	public Color shootPowerBarColour;
	public Color shootPowerBarFillColour;
	public Color passPowerBarColour;
	public Color passPowerBarFillColour;

	public GameObject playerHighlight;
	public Animator playerAnimator;

	private PlayerController playerController;
	private PlayerModel playerModel;
	private bool isMoving = false;

	// Use this for initialization
	void Start() {
		playerController = GetComponent<PlayerController>();
		playerModel = GetComponent<PlayerModel>();
	}

	// Update is called once per frame
	void Update() {
		float speed = playerModel.activeInputMagnitude;
		playerAnimator.SetBool("hasPuck", playerController.HasPuck());
		if (speed > 0.5f && !isMoving) {
			isMoving = true;
			playerAnimator.SetTrigger("start");
		} else if (speed <= 0.5f && isMoving == true) {
			isMoving = false;
			playerAnimator.SetTrigger("stop");
		}
	}

	public void ShowNoPuckControls() {
		if (shootButton != null) {
			shootButton.SetActive(false);
			passButton.SetActive(false);

			hitButton.SetActive(true);
			switchPlayerButton.SetActive(true);
		}
	}

	public void ShowPuckControls() {
		if (hitButton != null) {
			hitButton.SetActive(false);
			switchPlayerButton.SetActive(false);

			shootButton.SetActive(true);
			passButton.SetActive(true);
		}
	}

	public void EnablePowerBar(ShotType _shotType) {
		if (powerBar != null) {
			powerBar.gameObject.SetActive(true);
			powerBarFill.gameObject.SetActive(true);

			if (_shotType == ShotType.PASS) {
				powerBar.color = passPowerBarColour;
				powerBarFill.color = passPowerBarFillColour;
			} else if (_shotType == ShotType.SHOT) {
				powerBar.color = shootPowerBarColour;
				powerBarFill.color = shootPowerBarFillColour;
			}
		}
	}

	public void UpdatePowerBar(float progress) {
		if (powerBarFill != null) {
			powerBarFill.rectTransform.sizeDelta = new Vector2(maxPowerBarFillWidth * Mathf.Min(progress, 1.0f), powerBarFill.rectTransform.sizeDelta.y);
		}
	}

	public void DisablePowerBar() {
		if (powerBarFill != null) {
			powerBarFill.gameObject.SetActive(false);
			powerBar.gameObject.SetActive(false);
		}
	}

	public void EnablePlayerHighlight() {
		if (playerHighlight != null) {
			playerHighlight.SetActive(true);
		}
	}

	public void DisablePlayerHighlight() {
		if (playerHighlight != null) {
			playerHighlight.SetActive(false);
		}
	}
}
