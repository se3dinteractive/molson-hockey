﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IPuckEventListenerInterface {
	void OnZoneEnter(PuckEventManager.IceZone zone);

	void OnZoneExit(PuckEventManager.IceZone zone);

	void OnClaimed(int _teamLayer);

	void OnShot(int _teamLayer);

	void OnReset();
}

public class PuckEventManager : MonoBehaviour {

	public enum IceZone {
		NONE,
		HOME_ICING,
		AWAY_ICING,
		HOME_DEFENSE,
		AWAY_DEFENSE,
		HOME_NEUTRAL,
		AWAY_NEUTRAL
	};

	enum PuckEventType {
		ZONE_ENTER,
		ZONE_EXIT,
		ON_CLAIMED,
		ON_SHOT,
		ON_RESET
	};

	IceZone currentZone;
	int currentTeamLayer;

	List<IPuckEventListenerInterface> listeners;

	PuckController puckController;
	GameTimer gameTimer;

	// Use this for initialization
	void Start() {
		gameTimer = FindObjectOfType<GameTimer>();
		puckController = GetComponent<PuckController>();
		if (listeners == null) {
			listeners = new List<IPuckEventListenerInterface>();
		}
		currentZone = IceZone.NONE;
		currentTeamLayer = -1;
	}

	public int GetCurrentZone() {
		return (int)currentZone;
	}

	public void ResetEvents() {
		currentTeamLayer = -1;
		currentZone = IceZone.NONE;
		SendEvent(PuckEventType.ON_RESET);
	}

	// Update is called once per frame
	void Update() {
		int newTeamLayer = puckController.GetParentLayer();
		if (newTeamLayer != currentTeamLayer) {
			if (newTeamLayer == -1) {
				SendEvent(PuckEventType.ON_SHOT);
				currentTeamLayer = newTeamLayer;
			} else {
				currentTeamLayer = newTeamLayer;
				SendEvent(PuckEventType.ON_CLAIMED);
			}
		}
	}

	void SendEvent(PuckEventType _type) {
		if (listeners != null) {
			for (int i = 0; i < listeners.Count; i++) {
				if (_type == PuckEventType.ZONE_ENTER) {
					listeners[i].OnZoneEnter(currentZone);
				} else if (_type == PuckEventType.ZONE_EXIT) {
					listeners[i].OnZoneExit(currentZone);
				} else if (_type == PuckEventType.ON_CLAIMED) {
					listeners[i].OnClaimed(currentTeamLayer);
				} else if (_type == PuckEventType.ON_SHOT) {
					listeners[i].OnShot(currentTeamLayer);
				} else if (_type == PuckEventType.ON_RESET) {
					listeners[i].OnReset();
				}
			}
		}
	}

	void SwitchZones(IceZone _newZone) {
		if (currentZone != _newZone) {
			if (currentZone != IceZone.NONE) {
				SendEvent(PuckEventType.ZONE_EXIT);
			}
			currentZone = _newZone;
			SendEvent(PuckEventType.ZONE_ENTER);
		}
	}

	void OnTriggerEnter(Collider _collider) {
		if (_collider.gameObject.layer == Config.HOME_TEAM_ZONE_LAYER || _collider.gameObject.layer == Config.AWAY_TEAM_ZONE_LAYER) {
			IceZone newZone = currentZone;
			int period = gameTimer.GetPeriod();
			if (_collider.gameObject.tag == "IcingZone") {
				//newZone = (_collider.gameObject.layer == Config.HOME_TEAM_LAYER) ? IceZone.HOME_ICING : IceZone.AWAY_ICING;
				if ((_collider.gameObject.layer == Config.HOME_TEAM_ZONE_LAYER && period != 2) || (_collider.gameObject.layer == Config.AWAY_TEAM_ZONE_LAYER && period == 2)) {
					newZone = IceZone.HOME_ICING;
				} else {
					newZone = IceZone.AWAY_ICING;
				}
			} else if (_collider.gameObject.tag == "DefenseZone") {
				//newZone = (_collider.gameObject.layer == Config.HOME_TEAM_LAYER) ? IceZone.HOME_DEFENSE : IceZone.AWAY_DEFENSE;
				if ((_collider.gameObject.layer == Config.HOME_TEAM_ZONE_LAYER && period != 2) || (_collider.gameObject.layer == Config.AWAY_TEAM_ZONE_LAYER && period == 2)) {
					newZone = IceZone.HOME_DEFENSE;
				} else {
					newZone = IceZone.AWAY_DEFENSE;
				}
			} else if (_collider.gameObject.tag == "NeutralZone") {
				//newZone = (_collider.gameObject.layer == Config.HOME_TEAM_LAYER) ? IceZone.HOME_NEUTRAL : IceZone.AWAY_NEUTRAL;
				if ((_collider.gameObject.layer == Config.HOME_TEAM_ZONE_LAYER && period != 2) || (_collider.gameObject.layer == Config.AWAY_TEAM_ZONE_LAYER && period == 2)) {
					newZone = IceZone.HOME_NEUTRAL;
				} else {
					newZone = IceZone.AWAY_NEUTRAL;
				}
			}

			SwitchZones(newZone);
		}
	}

	public void AddListener(IPuckEventListenerInterface _listener) {
		if (listeners == null) {
			listeners = new List<IPuckEventListenerInterface>();
		}
		listeners.Add(_listener);
	}
}
