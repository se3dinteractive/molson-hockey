﻿using UnityEngine;
using System.Collections;

public class RinkModel : MonoBehaviour {

	//"Right" is negative z, "Left" is positive
	//"Home" is negative x, "Away" is positive

	public GameTimer gameTimer;

	public int HomeTeamLayer = 10;
	public int AwayTeamLayer = 11;

	public Transform CenterIceRight;
	public Transform CenterIceLeft;

	public Transform HomeBlueLineRight;
	public Transform HomeBlueLineLeft;

	public Transform HomeIcingLineRight;
	public Transform HomeIcingLineLeft;

	public Transform HomeNet;

	public Transform AwayBlueLineRight;
	public Transform AwayBlueLineLeft;

	public Transform AwayIcingLineRight;
	public Transform AwayIcingLineLeft;

	public Transform AwayNet;

	bool switchEnds = false;

	// Use this for initialization
	void Start() {
		if (gameTimer == null) {
			gameTimer = FindObjectOfType<GameTimer>();
		}
	}

	// Update is called once per frame
	void Update() {
		switchEnds = gameTimer.GetPeriod() == 2;
	}

	public Vector3 GetCenterIcePosition(float _negZWeight) {
		return CenterIceRight.position * _negZWeight + CenterIceLeft.position * (1.0f-_negZWeight);
	}

	public Vector3 GetAttackingBlueLinePosition(int _teamLayer, float _negZWeight) {
		if ((_teamLayer == HomeTeamLayer && switchEnds) || (_teamLayer == AwayTeamLayer && !switchEnds)) {
			return HomeBlueLineRight.position * _negZWeight + HomeBlueLineLeft.position * (1.0f - _negZWeight);
		} else {
			return AwayBlueLineRight.position * _negZWeight + AwayBlueLineLeft.position * (1.0f - _negZWeight);
		}
	}

	public Vector3 GetDefendingBlueLinePosition(int _teamLayer, float _negZWeight) {
		if ((_teamLayer == HomeTeamLayer && switchEnds) || (_teamLayer == AwayTeamLayer && !switchEnds)) {
			return AwayBlueLineRight.position * _negZWeight + AwayBlueLineLeft.position * (1.0f - _negZWeight);
		} else {
			return HomeBlueLineRight.position * _negZWeight + HomeBlueLineLeft.position * (1.0f - _negZWeight);
		}
	}

	public Vector3 GetAttackingIcingLinePosition(int _teamLayer, float _negZWeight) {
		if ((_teamLayer == HomeTeamLayer && switchEnds) || (_teamLayer == AwayTeamLayer && !switchEnds)) {
			return HomeIcingLineRight.position * _negZWeight + HomeIcingLineLeft.position * (1.0f - _negZWeight);
		} else {
			return AwayIcingLineRight.position * _negZWeight + AwayIcingLineLeft.position * (1.0f - _negZWeight);
		}
	}

	public Vector3 GetDefendingIcingLinePosition(int _teamLayer, float _negZWeight) {
		if ((_teamLayer == HomeTeamLayer && switchEnds) || (_teamLayer == AwayTeamLayer && !switchEnds)) {
			return AwayIcingLineRight.position * _negZWeight + AwayIcingLineLeft.position * (1.0f - _negZWeight);
		} else {
			return HomeIcingLineRight.position * _negZWeight + HomeIcingLineLeft.position * (1.0f - _negZWeight);
		}
	}

	public Vector3 GetAttackingNetPosition(int _teamLayer) {
		return ((_teamLayer == HomeTeamLayer && switchEnds) || (_teamLayer == AwayTeamLayer && !switchEnds)) ? HomeNet.position : AwayNet.position;
	}

	public Vector3 GetDefendingNetPosition(int _teamLayer) {
		return ((_teamLayer == HomeTeamLayer && switchEnds) || (_teamLayer == AwayTeamLayer && !switchEnds)) ? AwayNet.position : HomeNet.position;
	}

	public Vector3 GetDefendingDirection(int _teamLayer) {
		return ((_teamLayer == HomeTeamLayer && switchEnds) || (_teamLayer == AwayTeamLayer && !switchEnds)) ? new Vector3(1, 0, 0) : new Vector3(-1, 0, 0);
	}
}
