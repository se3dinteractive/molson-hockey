﻿using UnityEngine;
using System.Collections;

public class PlayerModel : MonoBehaviour {

	public enum PlayerRole {
		LEFT_WING,
		CENTER,
		RIGHT_WING,
		LEFT_D,
		RIGHT_D,
		GOALIE
	};

	public float movementSpeed;
	public float movementSpeedDecayDuration;
	public float acceleration;
	public float curSpeedMod = 1.0f;
	public float activeInputMagnitude;

	public float rotationCorrectionSpeed;

	public float shotVerticalAngle;

	public float minShootSpeed;
	public float maxShootSpeed;
	public float passSpeed;
	public float maxShootWindUpTime;
	public float maxPassWindUpTime;

	public float hitSpeed;
	public float hitDuration;

	public float puckCooldownTime;

	public float aimingTimeScale;

	public Transform shooterCamTarget;

	public Transform puckParent;

	[HideInInspector]
	public int TeamID;

	public PlayerRole role;

	public GameObject layerChild;

	public int teamLayer;

	public bool useAimShot;

	[HideInInspector]
	public bool isBeingHit;

	// Use this for initialization
	void Start() {
		
	}

	// Update is called once per frame
	void Update() {

	}

	public void OnReceiveHit() {
		isBeingHit = true;
	}

	public void OnHitEnd() {
		isBeingHit = false;
	}
}
