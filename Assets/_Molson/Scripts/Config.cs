﻿using UnityEngine;
using System.Collections;

public class Config {

	static public int PUCK_LAYER = 9;
	static public int HOME_TEAM_LAYER = 10;
	static public int AWAY_TEAM_LAYER = 11;
	static public int HOME_TEAM_ZONE_LAYER = 14;
	static public int AWAY_TEAM_ZONE_LAYER = 15;

	static public bool isOnline = true;

}
