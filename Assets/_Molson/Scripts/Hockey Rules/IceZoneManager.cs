﻿using UnityEngine;
using System.Collections;

public class IceZoneManager : MonoBehaviour {

	public IceZoneController homeIcing;
	public IceZoneController homeDefense;
	public IceZoneController homeNeutral;
	public IceZoneController awayNeutral;
	public IceZoneController awayDefense;
	public IceZoneController awayIcing;

	GameTimer timer;
	int period;

	// Use this for initialization
	void Start() {
		timer = FindObjectOfType<GameTimer>();
	}

	// Update is called once per frame
	void Update() {
		period = timer.GetPeriod();
	}

	public IceZoneController GetHomeIcingZone() {
		return (period != 2) ? homeIcing : awayIcing;
	}

	public IceZoneController GetHomeDefenseZone() {
		return (period != 2) ? homeDefense : awayDefense;
	}

	public IceZoneController GetHomeNeutralZone() {
		return (period != 2) ? homeNeutral : awayNeutral;
	}

	public IceZoneController GetAwayNeutralZone() {
		return (period != 2) ? awayNeutral : homeNeutral;
	}

	public IceZoneController GetAwayDefenseZone() {
		return (period != 2) ? awayDefense : homeDefense;
	}

	public IceZoneController GetAwayIcingZone() {
		return (period != 2) ? awayIcing : homeIcing;
	}

	public int GetHomePlayerInOffensiveZoneCount() {
		return GetAwayIcingZone().GetHomeTeamPlayerCount() + GetAwayDefenseZone().GetHomeTeamPlayerCount();
	}

	public int GetAwayPlayerInOffensiveZoneCount() {
		return GetHomeIcingZone().GetAwayTeamPlayerCount() + GetHomeDefenseZone().GetAwayTeamPlayerCount();
	}
}
