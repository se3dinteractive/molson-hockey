﻿using UnityEngine;
using System.Collections;

public class Faceoff : MonoBehaviour {

	public TeamController homeTeam;
	public TeamController awayTeam;

	public GameObject iceZoneParent;

	public GameObject[] homeTargetPositions;
	public GameObject[] awayTargetPositions;

	public GameObject puck;

	public float faceoffDelay = 1.0f;

	bool delayingFaceoff = false;
	float elapsedDelayTime = 0;

	GameTimer gameTimer;
	int homeActivePlayer;
	int awayActivePlayer;

	// Use this for initialization
	void Start() {
		gameTimer = FindObjectOfType<GameTimer>();
	}

	// Update is called once per frame
	void Update() {
		if (delayingFaceoff) {
			elapsedDelayTime += Time.deltaTime;
			if (elapsedDelayTime >= faceoffDelay) {
				delayingFaceoff = false;
				puck.SetActive(true);
				
				puck.GetComponent<Rigidbody>().isKinematic = false;
			}
		}
	}

	public void SetupFaceoff() {

		if (puck == null) {
			puck = GameObject.Find("Puck");
		}

		if (puck == null) {
			puck = GameObject.Find("Puck(Clone)");
		}

		iceZoneParent.BroadcastMessage("ResetCount");

		Rigidbody rigidbody;

		rigidbody = puck.GetComponent<Rigidbody>();
		puck.GetComponent<PuckController>().ForceFree();
		rigidbody.isKinematic = true;
		puck.SetActive(false);
		puck.transform.rotation = Quaternion.identity;
		puck.transform.position = new Vector3(transform.position.x, 1.0f, transform.position.z);
		puck.GetComponent<PuckEventManager>().ResetEvents();



		delayingFaceoff = true;
		elapsedDelayTime = 0;

		if (gameTimer == null) {
			gameTimer = FindObjectOfType<GameTimer>();
		}
		int period = gameTimer.GetPeriod();

		PlayerController[] playerControllers = homeTeam.GetPlayerControllers();
		PlayerModel[] playerModels = homeTeam.GetPlayerModels();

		for (int i = 0; i < playerControllers.Length; i++) {
			if (playerModels[i].role != PlayerModel.PlayerRole.GOALIE) {
				playerControllers[i].PrepareForFaceoff((period != 2) ? homeTargetPositions[(int)playerModels[i].role].transform : awayTargetPositions[(int)playerModels[i].role].transform);
			} else {
				GoalieController controller = playerModels[i].GetComponent<GoalieController>();
				if (controller) {
					controller.transform.position = controller.startPos;
				}
			}
		}

		homeTeam.ResetAllAI();

		playerModels = awayTeam.GetPlayerModels();
		playerControllers = awayTeam.GetPlayerControllers();
		

		for (int i = 0; i < playerControllers.Length; i++) {
			if (playerModels[i].role != PlayerModel.PlayerRole.GOALIE) {
				playerControllers[i].PrepareForFaceoff((period == 2) ? homeTargetPositions[(int)playerModels[i].role].transform : awayTargetPositions[(int)playerModels[i].role].transform);
			} else {
				GoalieController controller = playerModels[i].GetComponent<GoalieController>();
				if (controller) {
					controller.transform.position = controller.startPos;
				}
			}
		}

		awayTeam.ResetAllAI();

		homeTeam.MakeCenterActive();
		awayTeam.MakeCenterActive();
	}
}
