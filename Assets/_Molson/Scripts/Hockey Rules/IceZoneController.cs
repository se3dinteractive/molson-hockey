﻿using UnityEngine;
using System.Collections;

public class IceZoneController : MonoBehaviour {

	public int homeTeamPlayerCount = 0;
	public int awayTeamPlayerCount = 0;

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {

	}

	void OnTriggerEnter(Collider _collider) {
		if (_collider.gameObject.layer == 10) {
			homeTeamPlayerCount++;
		} else if (_collider.gameObject.layer == 11) {
			awayTeamPlayerCount++;
		}
	}

	void OnTriggerExit(Collider _collider) {
		if (_collider.gameObject.layer == 10) {
			homeTeamPlayerCount--;
		} else if (_collider.gameObject.layer == 11) {
			awayTeamPlayerCount--;
		}
	}

	public int GetHomeTeamPlayerCount() {
		return homeTeamPlayerCount;
	}

	public int GetAwayTeamPlayerCount() {
		return awayTeamPlayerCount;
	}

	public void ResetCount() {
		homeTeamPlayerCount = awayTeamPlayerCount = 0;
	}
}
