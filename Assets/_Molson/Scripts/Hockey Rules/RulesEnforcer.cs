﻿using UnityEngine;
using System.Collections;

public class RulesEnforcer : MonoBehaviour, IPuckEventListenerInterface {

	//1 is pos Z, 2 is neg Z

	public enum FaceoffDots {
		CENTER,
		AWAY_NEUTRAL,
		AWAY_NEUTRAL_2,
		HOME_NEUTRAL,
		HOME_NEUTRAL_2,
		AWAY_DEFENSE,
		AWAY_DEFENSE_2,
		HOME_DEFENSE,
		HOME_DEFENSE_2
	};

	public bool icingEnabled = true;
	public bool offsideEnabled = true;

	public PuckController puckController;
	public PuckEventManager puckEventMgr;
	public Faceoff[] faceoffs;
	public IceZoneController awayDefenseZone;
	public IceZoneController awayIcingZone;
	public IceZoneController homeDefenseZone;
	public IceZoneController homeIcingZone;

	IceZoneManager iceZoneManager;

	PuckEventManager.IceZone currentZone;
	GameTimer timer;
	int period;

	int currentPossessionLayer;
	int lastTeamToPossess;

	bool isBeingMovedForFaceoff = false;
	bool homeTeamDelayedOffside = false;
	bool awayTeamDelayedOffside = false;

	bool shotFromOwnZone = false;

	GameController gameController;
	RefDisplayController refDisplayCont;

	FaceoffDots targetDot;
	bool isTimingOut;
	public float whistleToFaceoffDuration;
	float elapsedWhistleTime;

	// Use this for initialization
	void Start() {
		iceZoneManager = FindObjectOfType<IceZoneManager>();
		timer = FindObjectOfType<GameTimer>();
		period = 1;
	}

	public void Init(GameObject puck) {
		gameController = FindObjectOfType<GameController>();
		puckController = puck.GetComponent<PuckController>();
		puckEventMgr = puck.GetComponent<PuckEventManager>();
		puckEventMgr.AddListener(this);

		refDisplayCont = FindObjectOfType<RefDisplayController>();
	}

	public bool IsInOffensiveZone(int _teamLayer) {
		if (_teamLayer == Config.HOME_TEAM_LAYER) {
			return (currentZone == PuckEventManager.IceZone.AWAY_DEFENSE || currentZone == PuckEventManager.IceZone.AWAY_ICING);
		} else if (_teamLayer == Config.AWAY_TEAM_LAYER) {
			Log.output = currentZone.ToString();
			return (currentZone == PuckEventManager.IceZone.HOME_DEFENSE || currentZone == PuckEventManager.IceZone.HOME_ICING);
		}

		return false;
	}

	private void ScheduleFaceoff(FaceoffDots _target) {
		isTimingOut = true;
		elapsedWhistleTime = 0.0f;
		targetDot = _target;
		timer.PauseGame();
	}

	// Update is called once per frame
	void Update() {
		if (isTimingOut) {
			elapsedWhistleTime += Time.deltaTime;
			if (elapsedWhistleTime >= whistleToFaceoffDuration) {
				isTimingOut = false;
				StartFaceoff(targetDot);
			}
		} else {
			period = timer.GetPeriod();

			if (homeTeamDelayedOffside) {
				if ((iceZoneManager.GetHomePlayerInOffensiveZoneCount() == 0) || currentPossessionLayer == Config.AWAY_TEAM_LAYER) {
					homeTeamDelayedOffside = false;
					refDisplayCont.Hide();
				}
			}

			if (awayTeamDelayedOffside) {
				if ((iceZoneManager.GetAwayPlayerInOffensiveZoneCount() == 0) || currentPossessionLayer == Config.HOME_TEAM_LAYER) {
					awayTeamDelayedOffside = false;
					refDisplayCont.Hide();
				}
			}
		}
	}

	int Rand2() {
		return Random.Range(0, 2);
	}

	void StartFaceoff(FaceoffDots _target) {
		refDisplayCont.Hide();
		timer.ResumeGame();
		awayTeamDelayedOffside = homeTeamDelayedOffside = false;
		if ((Config.isOnline && Network.isServer) || !Config.isOnline) {
			_target = GetFaceoffDot(_target);
			int offset = 0;
			if (_target == FaceoffDots.AWAY_DEFENSE || _target == FaceoffDots.AWAY_NEUTRAL ||
				_target == FaceoffDots.HOME_DEFENSE || _target == FaceoffDots.HOME_NEUTRAL) {
				offset = Rand2();
			}

			gameController.StartFaceoff((int)_target + offset);
		}
	}

	void TestForIcing(PuckEventManager.IceZone _zone) {
		if (icingEnabled && puckController.GetParentLayer() == -1 &&		// The puck isn't being controlled
			_zone == PuckEventManager.IceZone.HOME_ICING && // The puck has entered the home team's icing zone
			lastTeamToPossess == Config.AWAY_TEAM_LAYER &&  // The last team to possess the puck was the away team
			shotFromOwnZone) {								// The puck was released while in the away team's end of the rink
			ScheduleFaceoff(FaceoffDots.AWAY_DEFENSE);
			refDisplayCont.ShowIcing();
		} else if (icingEnabled && puckController.GetParentLayer() == -1 &&
			_zone == PuckEventManager.IceZone.AWAY_ICING &&
			lastTeamToPossess == Config.HOME_TEAM_LAYER &&
			shotFromOwnZone) {
			ScheduleFaceoff(targetDot = FaceoffDots.HOME_DEFENSE);
			refDisplayCont.ShowIcing();
		}
	}

	void TestForOffside(PuckEventManager.IceZone _zone) {
		if (offsideEnabled && _zone == PuckEventManager.IceZone.HOME_DEFENSE && currentZone == PuckEventManager.IceZone.HOME_NEUTRAL && ((iceZoneManager.GetAwayPlayerInOffensiveZoneCount() > 0) || awayTeamDelayedOffside)) {
			if (currentPossessionLayer == Config.AWAY_TEAM_LAYER) {
				ScheduleFaceoff(FaceoffDots.HOME_NEUTRAL);
				refDisplayCont.ShowOffside();
			}
		} else if (offsideEnabled && _zone == PuckEventManager.IceZone.AWAY_DEFENSE && currentZone == PuckEventManager.IceZone.AWAY_NEUTRAL && ((iceZoneManager.GetHomePlayerInOffensiveZoneCount() > 0) || homeTeamDelayedOffside)) {
			if (currentPossessionLayer == Config.HOME_TEAM_LAYER) {
				ScheduleFaceoff(FaceoffDots.AWAY_NEUTRAL);
				refDisplayCont.ShowOffside();
			}
		}
	}

	void TestForDelayedOffside(PuckEventManager.IceZone _zone) {
		if (offsideEnabled && currentPossessionLayer == Config.AWAY_TEAM_LAYER && (homeDefenseZone.GetAwayTeamPlayerCount() > 0 || homeIcingZone.GetAwayTeamPlayerCount() > 0)) {
			awayTeamDelayedOffside = true;
			refDisplayCont.ShowDelayedOffside();
		} else if (offsideEnabled && currentPossessionLayer == Config.HOME_TEAM_LAYER && (awayDefenseZone.GetHomeTeamPlayerCount() > 0 || awayIcingZone.GetHomeTeamPlayerCount() > 0)) {
			homeTeamDelayedOffside = true;
			refDisplayCont.ShowDelayedOffside();
		}
	}

	public void OnZoneEnter(PuckEventManager.IceZone _zone) {
		if (!isBeingMovedForFaceoff) {
			if (_zone == PuckEventManager.IceZone.AWAY_ICING || _zone == PuckEventManager.IceZone.HOME_ICING) {
				TestForIcing(_zone);
			} else if (_zone == PuckEventManager.IceZone.HOME_DEFENSE || _zone == PuckEventManager.IceZone.AWAY_DEFENSE) {
				TestForOffside(_zone);
			} else if (_zone == PuckEventManager.IceZone.HOME_NEUTRAL || _zone == PuckEventManager.IceZone.AWAY_NEUTRAL) {
				TestForDelayedOffside(_zone);
			}
		}

		currentZone = _zone;
	}

	public void OnGoalieHold() {
		refDisplayCont.ShowHeldPuck();
		FaceoffDots target = (currentZone == PuckEventManager.IceZone.AWAY_DEFENSE) ? FaceoffDots.AWAY_DEFENSE : FaceoffDots.HOME_DEFENSE;
		ScheduleFaceoff(target);
	}

	public void OnZoneExit(PuckEventManager.IceZone _zone) {

	}

	public void OnClaimed(int _teamLayer) {
		currentPossessionLayer = _teamLayer;
		TestForOffside(currentZone);
	}

	public void OnShot(int _teamLayer) {
		currentPossessionLayer = -1;
		lastTeamToPossess = _teamLayer;

		if (_teamLayer == Config.HOME_TEAM_LAYER && 
			(currentZone == PuckEventManager.IceZone.HOME_DEFENSE || 
			 currentZone == PuckEventManager.IceZone.HOME_NEUTRAL ||
			 currentZone == PuckEventManager.IceZone.HOME_ICING)) {
			shotFromOwnZone = true;
		} else if (_teamLayer == Config.AWAY_TEAM_LAYER &&
			(currentZone == PuckEventManager.IceZone.AWAY_DEFENSE ||
			 currentZone == PuckEventManager.IceZone.AWAY_NEUTRAL ||
			 currentZone == PuckEventManager.IceZone.AWAY_ICING)) {
			shotFromOwnZone = true;
		} else {
			shotFromOwnZone = false;
		}
	}

	public void OnReset() {
		currentPossessionLayer = lastTeamToPossess = 0;
		isBeingMovedForFaceoff = false;
		homeTeamDelayedOffside = false;
		awayTeamDelayedOffside = false;

		shotFromOwnZone = false;

		currentZone = PuckEventManager.IceZone.NONE;
	}

	FaceoffDots GetFaceoffDot(FaceoffDots _target) {
		if (_target == FaceoffDots.AWAY_DEFENSE) {
			return (period != 2) ? FaceoffDots.AWAY_DEFENSE : FaceoffDots.HOME_DEFENSE;
		} else if (_target == FaceoffDots.AWAY_NEUTRAL) {
			return (period != 2) ? FaceoffDots.AWAY_NEUTRAL : FaceoffDots.HOME_NEUTRAL;
		} else if (_target == FaceoffDots.HOME_NEUTRAL) {
			return (period != 2) ? FaceoffDots.HOME_NEUTRAL : FaceoffDots.AWAY_NEUTRAL;
		} else if (_target == FaceoffDots.HOME_DEFENSE) {
			return (period != 2) ? FaceoffDots.HOME_DEFENSE : FaceoffDots.AWAY_DEFENSE;
		} else {
			return FaceoffDots.CENTER;
		}
	}
}
