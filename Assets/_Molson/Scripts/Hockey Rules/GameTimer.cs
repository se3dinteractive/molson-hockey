﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameTimer : MonoBehaviour {

	public int numPeriods = 3;
	public int periodLength = 5;

	public string[] periodLabels;

	public Text timeLabel;
	public Text periodLabel;
	public Text homeScoreLabel;
	public Text awayScoreLabel;

	public Faceoff centerFaceoff;

	int period;
	int timeRemaining;
	float elapsedTimeSinceLastTick = 0;
	bool hasGameStarted = false;
	bool isGamePaused = false;

	// Use this for initialization
	void Start() {

	}

	public void StartGame() {
		period = 1;
		StartPeriod();
		DisplayTime();
		DisplayPeriod();

		hasGameStarted = true;
	}

	// Update is called once per frame
	void Update() {
		if (hasGameStarted && !isGamePaused) {
			elapsedTimeSinceLastTick += Time.deltaTime;
			if (elapsedTimeSinceLastTick >= 1.0f) {
				elapsedTimeSinceLastTick -= 1.0f;
				timeRemaining--;
				if (timeRemaining <= 0) {
					period++;
					if (period > numPeriods) {
						Debug.Log("GAME OVER");
					} else {
						StartPeriod();
						DisplayPeriod();
					}
				}

				DisplayTime();
			}
		}
	}

	public bool IsGamePaused() {
		return isGamePaused;
	}

	public void PauseGame() {
		isGamePaused = true;
	}

	public void ResumeGame() {
		isGamePaused = false;
	}

	void StartPeriod() {
		timeRemaining = periodLength * 60;
		centerFaceoff.SetupFaceoff();
	}

	void DisplayTime() {
		int minutesRemaining = timeRemaining / 60;
		int secondsRemaining = timeRemaining % 60;

		timeLabel.text = minutesRemaining.ToString() + ":" + ((secondsRemaining < 10) ? "0" + secondsRemaining.ToString() : secondsRemaining.ToString());
	}

	void DisplayPeriod() {
		periodLabel.text = periodLabels[period - 1];
	}

	public int GetPeriod() {
		return period;
	}

	public void ScoreGoal(int _netLayer) {
		if (_netLayer == Config.HOME_TEAM_LAYER) {
			awayScoreLabel.text = "" + (int.Parse(awayScoreLabel.text) + 1);
		} else if (_netLayer == Config.AWAY_TEAM_LAYER) {
			homeScoreLabel.text = "" + (int.Parse(homeScoreLabel.text) + 1);
		}
	}
}
