﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GoalDetection : MonoBehaviour {

	public bool isHomeNet;

	GameController gameController;
	GameTimer gameTimer;

	// Use this for initialization
	void Start() {
		gameTimer = FindObjectOfType<GameTimer>();
	}

	// Update is called once per frame
	void Update() {

	}

	void OnTriggerEnter(Collider _collider) {
		if (!gameTimer.IsGamePaused() && ((Config.isOnline && Network.isServer) || !Config.isOnline)) {
			if (_collider.gameObject.layer == 9) {
				if (gameController == null) {
					gameController = FindObjectOfType<GameController>();
				}
				gameController.ScoreGoal((isHomeNet) ? Config.HOME_TEAM_LAYER : Config.AWAY_TEAM_LAYER);
			}
		}
	}
}
