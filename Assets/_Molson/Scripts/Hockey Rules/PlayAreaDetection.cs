﻿using UnityEngine;
using System.Collections;

public class PlayAreaDetection : MonoBehaviour {

	GameController controller;

	// Use this for initialization
	void Start() {

	}

	// Update is called once per frame
	void Update() {

	}

	void OnTriggerExit(Collider _coll) {
		if ((Config.isOnline && Network.isServer) || !Config.isOnline) {
			if (_coll.gameObject.layer == 9) {
				if (controller == null) {
					controller = FindObjectOfType<GameController>();
				}
				controller.StartFaceoff(0);
			}
		}
	}
}
