﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(SproutServerManager))]
public class SproutServerManagerInspector : Editor {

	private SproutServerManager manager;

	public override void OnInspectorGUI() {
		manager = target as SproutServerManager;

		if (manager.serverProfiles == null) {
			manager.serverProfiles = new ServerProfile[0];
			AddProfile(manager);
		}

		int newTimeout = EditorGUILayout.IntField("Connection Test Timeout:", (int)manager.connectionTestTimeout);
		if (newTimeout < 0) {
			newTimeout = 0;
		}
		if (newTimeout != manager.connectionTestTimeout) {
			manager.connectionTestTimeout = (uint)newTimeout;
		}
		manager.serverTimeout = EditorGUILayout.FloatField("Timeout Length:", manager.serverTimeout);
		manager.activeProfile = EditorGUILayout.Popup(manager.activeProfile, GetProfileNames(manager));

		if (manager.activeProfile >= 0 && manager.activeProfile < manager.serverProfiles.Length) {
			EditorGUILayout.LabelField(manager.serverProfiles[manager.activeProfile].profileName, EditorStyles.boldLabel);
			manager.serverProfiles[manager.activeProfile].profileName = EditorGUILayout.TextField("Profile Name:", manager.serverProfiles[manager.activeProfile].profileName);
			manager.serverProfiles[manager.activeProfile].messageServerAddress = EditorGUILayout.TextField("Message Server Address:", manager.serverProfiles[manager.activeProfile].messageServerAddress);
			manager.serverProfiles[manager.activeProfile].statsServerAddress = EditorGUILayout.TextField("Stats Server Address:", manager.serverProfiles[manager.activeProfile].statsServerAddress);
			manager.serverProfiles[manager.activeProfile].appName = EditorGUILayout.TextField("App Name:", manager.serverProfiles[manager.activeProfile].appName);
		}

		if (GUILayout.Button("Create New Profile")) {
			manager.activeProfile = AddProfile(manager);
		}

		if (GUILayout.Button("Remove Current Profile")) {
			RemoveProfile(manager);
		}
	}

	private string[] GetProfileNames(SproutServerManager _manager) {
		string[] names = new string[_manager.serverProfiles.Length];
		for (int i = 0; i < names.Length; i++) {
			names[i] = _manager.serverProfiles[i].profileName;
		}
		return names;
	}

	private int AddProfile(SproutServerManager _manager) {
		ServerProfile[] oldProfiles = _manager.serverProfiles;
		_manager.serverProfiles = new ServerProfile[_manager.serverProfiles.Length + 1];
		int index;
		for (index = 0; index < oldProfiles.Length; index++) {
			_manager.serverProfiles[index] = oldProfiles[index];
		}

		ServerProfile profile = new ServerProfile();
		profile.profileName = "New Profile";
		_manager.serverProfiles[index] = profile;

		return index;
	}

	private void RemoveProfile(SproutServerManager _manager) {
		if (_manager.serverProfiles.Length > 0) {
			ServerProfile[] oldProfiles = _manager.serverProfiles;
			_manager.serverProfiles = new ServerProfile[_manager.serverProfiles.Length - 1];
			int index;
			int newIndex = 0;
			for (index = 0; index < oldProfiles.Length; index++) {
				if (index != _manager.activeProfile) {
					_manager.serverProfiles[newIndex++] = oldProfiles[index];
				}
			}
			_manager.activeProfile = 0;
		}
	}
}
