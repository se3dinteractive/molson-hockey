﻿/**
 * SproutAnalyticsReporter.cs
 * 
 * Handles reporting analytics events to the
 * Sprout system.
 * 
 **/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SproutAnalyticsReporter : MonoBehaviour, SeedAnalyticsReporter, SproutServerResponseHandler, SproutServerEventListener {

	public bool isReportingActive = true;		/**< If set to false, no information will be sent. Useful when running tests. **/

	private string sessionID;					/**< Server-reported id for this session. **/
	private SproutServerManager serverManager;	/**< Instance of the server manager used for sending info to the server. **/
	private bool isOnline;						/**< True when we can reach the stats server. **/
	private bool isSessionStarted;

	void Start() {
		SeedAnalytics.GetInstance().AddReporter(this);
		serverManager = FindObjectOfType<SproutServerManager>();
		serverManager.AddEventListener(this);
		sessionID = "";
	}

	void Update() {

	}

	void OnDisable() {
		StopSession();
	}

	public void StartSession() {
		if (!isSessionStarted) {
			WWWForm form = new WWWForm();
			AddTimeToForm("session_start_time", form);
			SendStat(form);
			isSessionStarted = true;
		}
	}

	public void ReportEvent(string _name) {
		WWWForm form = new WWWForm();
		form.AddField("event_name", _name);
		AddTimeToForm("event_time", form);
		SendStat(form);
	}

	public void ReportTiming(string _name, double _duration) {
		WWWForm form = new WWWForm();
		form.AddField("timing_name", _name);
		form.AddField("timing_duration", ""+_duration);
		AddTimeToForm("timing_start_time", form);
		SendStat(form);
	}

	public void ReportException(string _name, string _message, bool _isFatal) {
		WWWForm form = new WWWForm();
		form.AddField("exception_name", _name);
		form.AddField("exception_msg", _message);
		form.AddField("exception_fatal", (_isFatal) ? 1 : 0);
		form.AddField("os", "dummy_os");
		AddTimeToForm("exception_time", form);
		SendStat(form);
	}

	public void StopSession() {
		if (isSessionStarted) {
			WWWForm form = new WWWForm();
			form.AddField("user_id", SystemInfo.deviceUniqueIdentifier);
			form.AddField("app_name", serverManager.serverProfiles[serverManager.activeProfile].appName);
			form.AddField("session_id", sessionID);
			AddTimeToForm("session_end_time", form);
			serverManager.RushSendData(form, SproutServerRequestType.STATS);
			//StartCoroutine("SendStat", form);
			isSessionStarted = false;
		}
	}

	private void AddTimeToForm(string _fieldName, WWWForm _form) {
		_form.AddField(_fieldName, System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
	}

	private void SendStat(WWWForm _form) {
		if (isReportingActive) {
			_form.AddField("user_id", SystemInfo.deviceUniqueIdentifier);
			_form.AddField("app_name", serverManager.serverProfiles[serverManager.activeProfile].appName);

			if (isOnline) {
				serverManager.AddRequest(_form, this, SproutServerRequestType.STATS);
			} else {
				// save it for later
			}
		}
	}

	public void OnResponseReceived(string _response) {
		string[] splitMessage = _response.Split(' ');
		if (splitMessage[0] == "session_id") {
			sessionID = splitMessage[1];
		}
	}

	public void OnTimeout(SproutServerRequest _failedRequest) {

	}

	public void OnMessageServerStatusChange(ServerStatus _status) {

	}

	public void OnStatServerStatusChange(ServerStatus _status) {
		isOnline = (_status == ServerStatus.AVAILABLE);
	}
}
