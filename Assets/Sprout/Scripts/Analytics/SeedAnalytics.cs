﻿/**
 * SeedAnalytics.cs
 * 
 * Abstract wrapper for reporting analytics to 
 * various APIs. Each API should implement a
 * SeedAnalyticsReporter instance and register
 * it with this object. Then all events should
 * be reported through this object.
 * 
 **/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * Interface for API-specific reporting implementations. 
 **/
public interface SeedAnalyticsReporter {

	void StartSession();

	void StopSession();

	//void ReportScreen(string _name);

	void ReportEvent(string _name);

	void ReportTiming(string _name, double _duration);

	/*void ReportTransaction(string _name);

	void ReportSocialEvent(string _name);*/

	void ReportException(string _name, string _message, bool _isFatal);
};

public class AnalyticsTimer {
	public string name;
	public double duration;
};

public class SeedAnalytics {

	private static SeedAnalytics instance;

	private List<SeedAnalyticsReporter> reporters;
	private List<AnalyticsTimer> timers;

	public static SeedAnalytics GetInstance() {
		if (instance == null) {
			instance = new SeedAnalytics();
		}

		return instance;
	}

	protected SeedAnalytics() {
		reporters = new List<SeedAnalyticsReporter>();
		timers = new List<AnalyticsTimer>();
	}

	public void Update() {
		for (int i = 0; i < timers.Count; i++) {
			timers[i].duration += Time.deltaTime;
		}
	}

	public void AddReporter(SeedAnalyticsReporter _reporter) {
		reporters.Add(_reporter);
	}

	public void StartSession() {
		for (int i = 0; i < reporters.Count; i++) {
			reporters[i].StartSession();
		}
	}

	public void ReportEvent(string _name) {
		for (int i = 0; i < reporters.Count; i++) {
			reporters[i].ReportEvent(_name);
		}
	}

	public void ReportTiming(string _name, double _duration) {
		for (int i = 0; i < reporters.Count; i++) {
			reporters[i].ReportTiming(_name, _duration);
		}
	}

	public void StartTimer(string _name) {
		AnalyticsTimer timer = new AnalyticsTimer();
		timer.name = _name;
		timer.duration = 0;
		timers.Add(timer);
	}

	public void StopTimer(string _name) {
		for (int i = 0; i < timers.Count; i++) {
			if (timers[i].name == _name) {
				ReportTiming(_name, timers[i].duration);

				timers.Remove(timers[i]);
				break;
			}
		}
	}

	public void StopSession() {
		for (int i = 0; i < reporters.Count; i++) {
			reporters[i].StopSession();
		}
	}

	public void ReportException(string _name, string _message, bool _isFatal) {
		for (int i = 0; i < reporters.Count; i++) {
			reporters[i].ReportException(_name, _message, _isFatal);
		}
	}
}