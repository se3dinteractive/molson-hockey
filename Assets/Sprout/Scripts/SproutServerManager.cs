﻿/**
 * SproutServerManager.cs
 * 
 * Handles server profiles, server
 * connectivity, and communicating
 * with servers and reportng on 
 * thier status.
 * 
 **/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Text;

/**
 * Interface for responding to responses
 * from the server. OnResponseReceived
 * being called doesn't guarantee a 
 * success; error checking must be done
 * in each implementation.
 * **/
public interface SproutServerResponseHandler {
	void OnResponseReceived(string _response);
	void OnTimeout(SproutServerRequest _failedRequest);
};

/**
 * Interface for responding to server 
 * status events. Note that these are 
 * not continuous alerts; TestConnections
 * must be called before this interface 
 * is used. Also note that all functions
 * will be called once when registering 
 * a new interface to inform the new 
 * listener of the current server status.
 * **/
public interface SproutServerEventListener {
	void OnMessageServerStatusChange(ServerStatus _status);
	void OnStatServerStatusChange(ServerStatus _status);
};

public enum ServerStatus {
	UNKNOWN,		/**< Default value. **/
	AVAILABLE,		/**< Server is available. **/
	UNAVAILABLE		/**< Server is not reachable. **/
};

/**
 * Represents one schema for this
 * app. Having multiple profiles
 * allows for multiple set ups (e.g.
 * having a testing profile with
 * a local server, and a production
 * profile with a web server).
 * **/
[System.Serializable]
public struct ServerProfile {
	public string profileName;
	public string messageServerAddress;
	public string statsServerAddress;
	public string appName;
	public ServerStatus messageServerStatus;
	public ServerStatus statsServerStatus;
};

public enum SproutServerRequestType {
	APP_ACCESS,
	STATS
};

/**
 * Represents a request to be sent to the server.
 * **/
public class SproutServerRequest {
	public WWWForm data;									/**< Form with POST data to send to the server. **/
	public SproutServerResponseHandler responseHandler;		/**< Interface to be notified when this request is finished. **/
	public SproutServerRequestType requestType;				/**< Indicates which server to send this request to. **/
	public float elapsedTime;								/**< How long this request has been active. **/
	public string response;									/**< Server response. **/
	public IEnumerator coroutine;							/**< Coroutine used to make this request. Stored for easy cancelling in case of timeout. **/
};

[System.Serializable]
public class SproutServerManager : MonoBehaviour {

	public uint connectionTestTimeout = 20;					/**< How long to do the connection tests for. Must have a value > 0. **/
	public float serverTimeout = -1.0f;						/**< How long to wait for requests to finish. -1 for no timeout. **/
	public int activeProfile;								/**< Which server profile is active. **/
		
	public ServerProfile[] serverProfiles;					/**< Collection of server profiles. **/

	private SproutServerRequest messageServerRequest;		/**< Request used to test message server. **/
	private SproutServerRequest statsServerRequest;			/**< Request used to test stats server. **/

	private List<SproutServerRequest> requests;				/**< Active requests. **/

	private List<SproutServerEventListener> eventListeners;	/**< Listeners for server status changes. **/

	// Use this for initialization
	void Start() {
		requests = new List<SproutServerRequest>();
		if (eventListeners == null) {
			eventListeners = new List<SproutServerEventListener>();
		}
		TestConnections();
	}

	private void TestConnections() {
		/*WWWForm dummyForm = new WWWForm();
		dummyForm.AddField("test", 1);

		messageServerRequest = new SproutServerRequest();
		messageServerRequest.data = dummyForm;
		messageServerRequest.requestType = SproutServerRequestType.APP_ACCESS;
		//messageServerRequest.response = new WWW(serverProfiles[activeProfile].messageServerAddress, dummyForm);
		//messageServerRequest.coroutine = TestMessageServer();
		//StartCoroutine(messageServerRequest.coroutine);

		statsServerRequest = new SproutServerRequest();
		statsServerRequest.data = dummyForm;
		//statsServerRequest.response = new WWW(serverProfiles[activeProfile].statsServerAddress, dummyForm);
		statsServerRequest.requestType = SproutServerRequestType.STATS;
		//statsServerRequest.coroutine = TestStatServer();
		//StartCoroutine(statsServerRequest.coroutine);*/
		serverProfiles[activeProfile].messageServerStatus = ServerStatus.AVAILABLE;
		DispatchMessageServerStatus();

		serverProfiles[activeProfile].statsServerStatus = ServerStatus.AVAILABLE;
		DispatchStatServerStatus();
	}

	// Update is called once per frame
	void Update() {
		/*for (int i = requests.Count - 1; i >= 0; i--) {
			if (requests[i].response.isDone) {
				requests.RemoveAt(i);
			} else {
				requests[i].elapsedTime += Time.deltaTime;
				if (serverTimeout > 0 && requests[i].elapsedTime >= serverTimeout) {
					//StopCoroutine(requests[i].coroutine);
					if (requests[i].responseHandler != null) {
						requests[i].responseHandler.OnTimeout(requests[i]);
					}
					requests.RemoveAt(i);
				}
			}
		}

		if (messageServerRequest != null) {
			messageServerRequest.elapsedTime += Time.deltaTime;
			if (messageServerRequest.elapsedTime >= connectionTestTimeout) {
				StopCoroutine(messageServerRequest.coroutine);
				serverProfiles[activeProfile].messageServerStatus = ServerStatus.UNAVAILABLE;
				DispatchMessageServerStatus();
				messageServerRequest = null;
			}
		}

		if (statsServerRequest != null) {
			statsServerRequest.elapsedTime += Time.deltaTime;
			if (statsServerRequest.elapsedTime >= connectionTestTimeout) {
				StopCoroutine(statsServerRequest.coroutine);
				serverProfiles[activeProfile].statsServerStatus = ServerStatus.UNAVAILABLE;
				DispatchStatServerStatus();
				statsServerRequest = null;
			}
		}*/
	}

	public void AddRequest(WWWForm _data, SproutServerResponseHandler _handler, SproutServerRequestType _requestType) {
		SproutServerRequest request = new SproutServerRequest();
		request.data = _data;
		request.responseHandler = _handler;
		request.requestType = _requestType;
		//request.coroutine = SendData(request);

		//StartCoroutine(request.coroutine);
		SendData(request);

		requests.Add(request);
	}

	public void RushSendData(WWWForm _data, SproutServerRequestType _requestType) {
		string url = (_requestType == SproutServerRequestType.APP_ACCESS) ? serverProfiles[activeProfile].messageServerAddress : serverProfiles[activeProfile].statsServerAddress;
		
		WebRequest request = WebRequest.Create(url);
		request.Method = "POST";
		request.ContentType = "application/x-www-form-urlencoded";
		request.ContentLength = _data.data.Length;

		Stream dataStream = request.GetRequestStream();
		dataStream.Write(_data.data, 0, _data.data.Length);
		dataStream.Close();

		WebResponse response = request.GetResponse();
		response.Close();
	}

	private void SendData(SproutServerRequest _request) {
		string url = (_request.requestType == SproutServerRequestType.APP_ACCESS) ? serverProfiles[activeProfile].messageServerAddress : serverProfiles[activeProfile].statsServerAddress;
		/*_request.response = new WWW(url, _request.data);
		yield return _request.response;

		if (_request.responseHandler != null) {
			_request.responseHandler.OnResponseReceived(_request.response);
		}*/

		WebRequest request = WebRequest.Create(url);
		request.Method = "POST";
		request.ContentType = "application/x-www-form-urlencoded";
		request.ContentLength = _request.data.data.Length;

		Stream dataStream = request.GetRequestStream();
		dataStream.Write(_request.data.data, 0, _request.data.data.Length);
		dataStream.Close();

		WebResponse response = request.GetResponse();

		dataStream = response.GetResponseStream();
		StreamReader reader = new StreamReader(dataStream);
		_request.response = reader.ReadToEnd();

		reader.Close();
		dataStream.Close();
		response.Close();

		if (_request.responseHandler != null) {
			_request.responseHandler.OnResponseReceived(_request.response);
		}
	}

	private void DispatchMessageServerStatus() {
		for (int i = 0; i < eventListeners.Count; i++) {
			eventListeners[i].OnMessageServerStatusChange(serverProfiles[activeProfile].messageServerStatus);
		}
	}

	private void DispatchStatServerStatus() {
		for (int i = 0; i < eventListeners.Count; i++) {
			eventListeners[i].OnStatServerStatusChange(serverProfiles[activeProfile].statsServerStatus);
		}
	}

	/*private IEnumerator TestMessageServer() {
		
		yield return messageServerRequest.response;

		if (messageServerRequest.response.isDone) {
			if (messageServerRequest.response.text == "Connection successful") {
				serverProfiles[activeProfile].messageServerStatus = ServerStatus.AVAILABLE;
			} else {
				serverProfiles[activeProfile].messageServerStatus = ServerStatus.UNAVAILABLE;
			}
			Debug.Log("Message server status: " + serverProfiles[activeProfile].messageServerStatus);
			DispatchMessageServerStatus();
		}
	}

	private IEnumerator TestStatServer() {
		yield return statsServerRequest.response;

		if (statsServerRequest.response.isDone) {
			if (statsServerRequest.response.text == "Connection successful") {
				serverProfiles[activeProfile].statsServerStatus = ServerStatus.AVAILABLE;
			} else {
				serverProfiles[activeProfile].statsServerStatus = ServerStatus.UNAVAILABLE;
			}
			Debug.Log("Stats server status: " + serverProfiles[activeProfile].statsServerStatus);
			DispatchStatServerStatus();
		}
	}*/

	public void AddEventListener(SproutServerEventListener _listener) {
		if (eventListeners == null) {
			eventListeners = new List<SproutServerEventListener>();
		}
		eventListeners.Add(_listener);
		_listener.OnMessageServerStatusChange(serverProfiles[activeProfile].messageServerStatus);
		_listener.OnStatServerStatusChange(serverProfiles[activeProfile].statsServerStatus);
	}
}
