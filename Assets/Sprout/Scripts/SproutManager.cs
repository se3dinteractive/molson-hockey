﻿/**
 * SproutManager.cs
 * 
 * Handles communicating with the message server
 * and any message-related events.
 * 
 **/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/** 
 * An interface for handling server messages
 * Your app should implement this interface 
 * to display and process messages recieved
 * from the server.
 * **/
public interface SproutMessageHandler {
	void OnMessageReceived(SproutMessage message);
	void OnEndOfMessages();
};

/**
 * Visible messages have info that should be
 * displayed to users, invisible messages only
 * have commands to be processed.
 **/
public enum SproutMessageType {
	VISIBLE,
	INVISIBLE
};

/**
 * Message from the server. 
 **/
public struct SproutMessage {
	public SproutMessageType type;
	public string command;
	public string title;
	public string body;
};

[RequireComponent(typeof(SproutServerManager))]
public class SproutManager : MonoBehaviour, SproutServerEventListener, SproutServerResponseHandler {

	public float locationDetectionTimeout = -1;			/**< How long to search for a location. -1 for no timeout. **/

	private List<SproutMessageHandler> messageHandlers; /**< List of active event interfaces. **/

	private string location;							/**< Coordinate location, or 'UNDEFINED' if no location could be obtained. **/
	private float elapsedLocationTime;					/**< Length of time we have been searching for a location. **/
	private bool isFindingLocation;						/**< True while searching for a location. **/
	private IEnumerator locationRequest;				/**< Location finding coroutine, stored for easily cancelling if timed out. **/

	private bool isMsgServerOnline;						/**< True if we can reach the message server. **/
	private bool isStatServerOnline;					/**< True if we can reach the stats server. **/
	private bool hasStarted;							/**< True once we have attempted to retrieve the messages once. **/

	private SproutServerManager serverManager;			/**< Server info and data sending interface. **/

	void Start() {
		messageHandlers = new List<SproutMessageHandler>();
		serverManager = GetComponent<SproutServerManager>();
		serverManager.AddEventListener(this);
	}

	void Update() {
		SeedAnalytics.GetInstance().Update();

		if (isFindingLocation && locationDetectionTimeout > 0) {
			elapsedLocationTime += Time.deltaTime;
			if (elapsedLocationTime >= locationDetectionTimeout) {
				StopCoroutine(locationRequest);
				EndLocationSearch();
			}
		}

		if (!hasStarted && isMsgServerOnline && isStatServerOnline) {
			locationRequest = FindLocation();
			isFindingLocation = true;
			StartCoroutine(locationRequest);
			hasStarted = true;
		}
	}

	/**
	 * Parses the response from the server, and prepares
	 * and dispatches the messages (if necessary)
	 **/
	void ProcessServerMessages(string _serverMessages) {

		string[] splitMsg = _serverMessages.Split(' ');
		int index = 0;
		int count = 0;
		SproutMessage message;
		while (index < splitMsg.Length) {
			message = new SproutMessage();
			// Process the message type
			if (splitMsg[index] == "VISIBLE") {
				message.type = SproutMessageType.VISIBLE;
			} else if (splitMsg[index] == "INVISIBLE") {
				message.type = SproutMessageType.INVISIBLE;
			} else {
				index++;
				continue;
			}
			index++;

			// Parse how many words there are in the command and read them
			count = int.Parse(splitMsg[index++]);
			for (int i = 0; i < count; i++) {
				message.command += splitMsg[index++];
				if (i < count - 1) message.command += " ";
			}

			// Parse how many words there are in the title and read them
			count = int.Parse(splitMsg[index++]);
			for (int i = 0; i < count; i++) {
				message.title += splitMsg[index++];
				if (i < count - 1) message.title += " ";
			}

			// Parse how many words there are in the body and read them
			count = int.Parse(splitMsg[index++]);
			for (int i = 0; i < count; i++) {
				message.body += splitMsg[index++];
				if (i < count - 1) message.body += " ";
			}

			DispatchMessage(message);
		}

		DispatchEndOfMessages();
	}

	public void AddMessageHandler(SproutMessageHandler listener) {
		messageHandlers.Add(listener);
	}

	void DispatchMessage(SproutMessage message) {
		for (int i = 0; i < messageHandlers.Count; i++) {
			messageHandlers[i].OnMessageReceived(message);
		}
	}

	void DispatchEndOfMessages() {
		for (int i = 0; i < messageHandlers.Count; i++) {
			messageHandlers[i].OnEndOfMessages();
		}
	}

	private void EndLocationSearch() {
		isFindingLocation = false;
		locationRequest = null;
		RequestMessages();
	}

	IEnumerator FindLocation() {
		elapsedLocationTime = 0;
		location = "Undetermined";
		if (Input.location.isEnabledByUser) {
			Input.location.Start();

			while (Input.location.status == LocationServiceStatus.Initializing) {
				yield return new WaitForSeconds(1);
			}

			if (Input.location.status == LocationServiceStatus.Running) {
				location = Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " ";
			}

			Input.location.Stop();
		}

		EndLocationSearch();
	}

	private void RequestMessages() {
		WWWForm form = new WWWForm();
		form.AddField("user_id", SystemInfo.deviceUniqueIdentifier);
		form.AddField("operating_system", "ANDROID");
		form.AddField("location", location);
		form.AddField("app_name", serverManager.serverProfiles[serverManager.activeProfile].appName);

		serverManager.AddRequest(form, this, SproutServerRequestType.APP_ACCESS);
	}

	public void OnResponseReceived(string _response) {
		ProcessServerMessages(_response);
	}

	public void OnTimeout(SproutServerRequest _failedRequest) {
		DispatchEndOfMessages();
	}

	public void OnMessageServerStatusChange(ServerStatus _status) {
		isMsgServerOnline = (_status == ServerStatus.AVAILABLE);
	}

	public void OnStatServerStatusChange(ServerStatus _status) {
		isStatServerOnline = (_status == ServerStatus.AVAILABLE);
	}
}
