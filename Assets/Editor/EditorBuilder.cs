﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;

public class EditorBuilder {

	static string APP_NAME = "Molson Hockey Tops";
	static string TARGET_DIR = "../Builds/Molson";

	[MenuItem ("Build/Build Android")]
	public static void PerformAndroidBuild() {
		string[] versionCode = PlayerSettings.bundleVersion.Split('.');
		int buildVersion = int.Parse(versionCode[2]);
		buildVersion++;
		PlayerSettings.bundleVersion = versionCode[0] + "." + versionCode[1] + "." + buildVersion.ToString();
		string target_dir = APP_NAME + ".apk";
		PlayerSettings.keyaliasPass = "se3dinteractive";
		PlayerSettings.keystorePass = "se3dinteractive";
		GenericBuild(FindEnabledEditorScenes(), TARGET_DIR + "/" + target_dir, BuildTarget.Android, BuildOptions.None);
	}

	[MenuItem ("Build/Build PC")]
	public static void PerformPCBuild() {
		string target_dir = APP_NAME + ".exe";
		GenericBuild(FindEnabledEditorScenes(), TARGET_DIR + "/" + target_dir, BuildTarget.StandaloneWindows, BuildOptions.None);
	}

	private static string[] FindEnabledEditorScenes() {
		List<string> EditorScenes = new List<string>();
		foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes) {
			if (!scene.enabled) continue;
			EditorScenes.Add(scene.path);
		}

		return EditorScenes.ToArray();
	}

	static void GenericBuild(string[] scenes, string target_dir, BuildTarget build_target, BuildOptions build_options) {
		EditorUserBuildSettings.SwitchActiveBuildTarget(build_target);
		string res = BuildPipeline.BuildPlayer(scenes, target_dir, build_target, build_options);
		if (res.Length > 0) {
			throw new Exception("BuildPlayer failure: " + res);
		}
	}
}
